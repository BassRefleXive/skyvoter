<?php

declare(strict_types = 1);

namespace App\Component\Captcha\CapMonsterApi;


use App\Component\Captcha\AntiCaptchaApi\NoCaptcha as BaseNoCaptcha;

class NoCaptcha extends BaseNoCaptcha
{
    protected function name(): string
    {
        return 'CapMonster';
    }
}