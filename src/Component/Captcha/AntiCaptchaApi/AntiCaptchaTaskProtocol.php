<?php

declare(strict_types = 1);

namespace App\Component\Captcha\AntiCaptchaApi;

interface AntiCaptchaTaskProtocol
{
    public function getPostData();

    public function getTaskSolution();
}