<?php

declare(strict_types = 1);

namespace App\Component\Captcha\AntiCaptchaApi;

use Psr\Log\LoggerInterface;

abstract class AntiCaptcha implements AntiCaptchaTaskProtocol
{
    private $logger;

    private $host;
    private $scheme;
    private $clientKey;
    private $verboseMode = false;
    private $errorMessage;
    public $taskInfo;

    public function __construct(string $host, string $scheme, LoggerInterface $logger)
    {
        $this->host = $host;
        $this->scheme = $scheme;

        $this->logger = $logger;
    }

    /**
     * Submit new task and receive tracking ID
     */
    public function createTask(): ?int
    {
        $postData = array(
            "clientKey" => $this->clientKey,
            "task" => $this->getPostData()
        );

        $submitResult = $this->jsonPostRequest("createTask", $postData);

        if ($submitResult == false) {
            $this->logger->error(sprintf('%s: API error', $this->name()));

            return null;
        }

        if ($submitResult->errorId == 0) {
            $this->logger->info(sprintf('%s: created task with ID %s', $this->name(), $submitResult->taskId));

            return (int) $submitResult->taskId;
        } else {
            $errorCode = 'null';
            $errorDescription = 'null';
            if (property_exists($submitResult, 'errorCode')) {
                $errorCode = $submitResult->errorCode;
            }

            if (property_exists($submitResult, 'errorDescription')) {
                $errorDescription = $submitResult->errorDescription;
            }
            $this->logger->error(sprintf('%s: API error %s : %s', $this->name(), $errorCode, $errorDescription));
            $this->setErrorMessage($submitResult->errorDescription);

            return null;
        }

    }

    public function waitForResult(int $taskId, $maxSeconds = 60, $currentSecond = 0)
    {
        $postData = array(
            "clientKey" => $this->clientKey,
            "taskId" => $taskId,
        );

        sleep(3);

        $this->logger->debug(sprintf('%s: Requesting task status. Task ID: %s', $this->name(), $taskId));
        $postResult = $this->jsonPostRequest("getTaskResult", $postData);

        if ($postResult == false) {
            $this->logger->error(sprintf('%s: API error. Task ID: %s', $this->name(), $taskId));

            return false;
        }

        $this->taskInfo = $postResult;


        if ($this->taskInfo->errorId == 0) {
            if ($this->taskInfo->status == "processing") {
                $this->logger->debug(sprintf('%s: Task is still processing. Task ID: %s', $this->name(), $taskId));

                //repeating attempt
                return $this->waitForResult($taskId, $maxSeconds, $currentSecond + 1);

            }
            if ($this->taskInfo->status == "ready") {
                $this->logger->info(sprintf('%s: Task is complete. Task ID: %s', $this->name(), $taskId));

                return true;
            }
            $this->setErrorMessage("unknown API status, update your software");

            return false;

        } else {
            $errorCode = 'null';
            $errorDescription = 'null';
            if (property_exists($this->taskInfo, 'errorCode')) {
                $errorCode = $this->taskInfo->errorCode;
            }

            if (property_exists($this->taskInfo, 'errorDescription')) {
                $errorDescription = $this->taskInfo->errorDescription;
            }

            $this->logger->error(sprintf('%s: API error %s : %s', $this->name(), $errorCode, $errorDescription));
            $this->setErrorMessage($errorDescription);

            return false;
        }
    }

    public function getBalance()
    {
        $postData = array(
            "clientKey" => $this->clientKey
        );
        $result = $this->jsonPostRequest("getBalance", $postData);
        if ($result == false) {
            $this->logger->error(sprintf('%s: API error', $this->name()));

            return false;
        }
        if ($result->errorId == 0) {
            return $result->balance;
        } else {
            return false;
        }
    }

    public function jsonPostRequest($methodName, $postData)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "{$this->scheme}://{$this->host}/$methodName");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_ENCODING, "gzip,deflate");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        $postDataEncoded = json_encode($postData);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postDataEncoded);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json; charset=utf-8',
            'Accept: application/json',
            'Content-Length: ' . strlen($postDataEncoded)
        ));
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        $result = curl_exec($ch);
        $curlError = curl_error($ch);

        if ($curlError != "") {
            $this->logger->error(sprintf('%s: Network error: %s', $this->name(), $curlError));

            return false;
        }
        curl_close($ch);

        return json_decode($result);
    }

    public function setVerboseMode($mode)
    {
        $this->verboseMode = $mode;
    }

    public function setErrorMessage($message)
    {
        $this->errorMessage = $message;
    }

    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    public function setHost($host)
    {
        $this->host = $host;
    }

    public function setScheme($scheme)
    {
        $this->scheme = $scheme;
    }

    /**
     * Set client access key, must be 32 bytes long
     * @param string $key
     */
    public function setKey($key)
    {
        $this->clientKey = $key;
    }

    abstract protected function name(): string;
}