<?php

declare(strict_types = 1);

namespace App\Component\Captcha;

use App\Component\Captcha\Enum\Resolver;
use App\Component\Captcha\Exception\ResolverRegistryException;

class ResolverRegistry
{
    private $resolvers = [];

    public function add(string $key, CaptchaResolverInterface $resolver): self
    {
        if (array_key_exists($key, $this->resolvers)) {
            throw ResolverRegistryException::alreadyExists($key);
        }

        $this->resolvers[$key] = $resolver;

        return $this;
    }

    public function get(Resolver $resolver): CaptchaResolverInterface
    {
        if (!array_key_exists($resolver->getValue(), $this->resolvers)) {
            throw ResolverRegistryException::notFound($resolver->getValue());
        }

        return $this->resolvers[$resolver->getValue()];
    }
}