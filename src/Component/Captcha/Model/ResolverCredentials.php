<?php

declare(strict_types=1);

namespace App\Component\Captcha\Model;


use App\Component\Captcha\Enum\Resolver;

class ResolverCredentials
{
    private $resolver;
    private $apiKey;

    public function __construct(Resolver $resolver, string $apiKey)
    {
        $this->resolver = $resolver;
        $this->apiKey = $apiKey;
    }

    public function resolver(): Resolver
    {
        return $this->resolver;
    }

    public function apiKey(): string
    {
        return $this->apiKey;
    }
}