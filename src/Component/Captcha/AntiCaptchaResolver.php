<?php

declare(strict_types = 1);

namespace App\Component\Captcha;

use App\Component\Captcha\AntiCaptchaApi\AntiCaptcha;
use App\Component\Captcha\AntiCaptchaApi\FunCaptcha;
use App\Component\Captcha\AntiCaptchaApi\ImageToText;
use App\Component\Captcha\Command\ResolveCommandInterface;
use App\Component\Captcha\Command\ResolveFunCaptchaCommand;
use App\Component\Captcha\Command\ResolveImageToTextCommand;
use App\Component\Captcha\Command\ResolveReCaptchaCommand;
use App\Component\Captcha\Exception\AntiCaptchaResolverException;
use App\Component\Captcha\Model\ResolverCredentials;
use Psr\Log\LoggerInterface;

class AntiCaptchaResolver implements CaptchaResolverInterface
{
    private $noCaptchaResolver;
    private $funCaptchaResolver;
    private $imageToTextResolver;
    private $logger;

    public function __construct(
        AntiCaptcha $noCaptchaResolver,
        FunCaptcha $funCaptchaResolver,
        ImageToText $imageToTextResolver,
        LoggerInterface $logger
    )
    {
        $this->noCaptchaResolver = $noCaptchaResolver;
        $this->funCaptchaResolver = $funCaptchaResolver;
        $this->imageToTextResolver = $imageToTextResolver;
        $this->logger = $logger;
    }

    /**
     * @param ResolverCredentials $credentials
     * @param ResolveReCaptchaCommand $command
     *
     * @return string
     */
    public function resolveReCaptcha(ResolverCredentials $credentials, ResolveCommandInterface $command): string
    {
        $this->logger->info('AntiCaptchaResolver Request', ['site_key' => $command->siteKey(), 'endpoint' => $command->endpoint()]);

        $this->noCaptchaResolver->setKey($credentials->apiKey());
        $this->noCaptchaResolver->setWebsiteURL($command->endpoint());
        $this->noCaptchaResolver->setWebsiteKey($command->siteKey());

        if (null === $taskId = $this->noCaptchaResolver->createTask()) {
            throw AntiCaptchaResolverException::createReCaptchaTaskFailed($command->siteKey(), $command->endpoint(), $this->noCaptchaResolver->getErrorMessage());
        }

        if (!$this->noCaptchaResolver->waitForResult($taskId)) {
            throw AntiCaptchaResolverException::waitResponseFailed($taskId);
        }

        return $this->noCaptchaResolver->getTaskSolution();
    }

    /**
     * @param ResolverCredentials $credentials
     * @param ResolveFunCaptchaCommand $command
     *
     * @return string
     */
    public function resolveFunCaptcha(ResolverCredentials $credentials, ResolveCommandInterface $command): string
    {
        $this->funCaptchaResolver->setKey($credentials->apiKey());
        $this->funCaptchaResolver->setWebsitePublicKey($command->publicKey());
        $this->funCaptchaResolver->setWebsiteURL($command->endpoint());
        $this->funCaptchaResolver->setUserAgent($command->userAgent());

        if (null === $taskId = $this->funCaptchaResolver->createTask()) {
            throw AntiCaptchaResolverException::createFunCaptchaTaskFailed($command->publicKey(), $command->endpoint(), $this->funCaptchaResolver->getErrorMessage());
        }

        if (!$this->funCaptchaResolver->waitForResult($taskId)) {
            throw AntiCaptchaResolverException::waitResponseFailed($taskId);
        }

        if (!is_string($this->funCaptchaResolver->getTaskSolution())) {
            throw AntiCaptchaResolverException::unexpectedFunCaptchaSolution();
        }

        return $this->funCaptchaResolver->getTaskSolution();
    }

    /**
     * @param ResolverCredentials $credentials
     * @param ResolveImageToTextCommand $command
     * @param string $image
     *
     * @return string
     */
    public function resolveImage(ResolverCredentials $credentials, ResolveCommandInterface $command): string
    {
        $this->imageToTextResolver->setKey($credentials->apiKey());
        $this->imageToTextResolver->setBody($command->image());

        if (null === $taskId = $this->funCaptchaResolver->createTask()) {
            throw AntiCaptchaResolverException::createImageToTextTaskFailed($this->funCaptchaResolver->getErrorMessage());
        }

        if (!$this->funCaptchaResolver->waitForResult($taskId)) {
            throw AntiCaptchaResolverException::waitResponseFailed($taskId);
        }

        return $this->funCaptchaResolver->getTaskSolution();
    }
}