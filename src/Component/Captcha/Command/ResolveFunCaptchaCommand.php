<?php

declare(strict_types=1);

namespace App\Component\Captcha\Command;

class ResolveFunCaptchaCommand implements ResolveCommandInterface
{
    private $publicKey;
    private $endpoint;
    private $userAgent;

    public function __construct(string $publicKey, string $endpoint)
    {
        $this->publicKey = $publicKey;
        $this->endpoint = $endpoint;
    }

    public function publicKey(): string
    {
        return $this->publicKey;
    }

    public function endpoint(): string
    {
        return $this->endpoint;
    }

    public function userAgent(): string
    {
        return $this->userAgent;
    }

    public function withUserAgent(string $userAgent): self
    {
        $this->userAgent = $userAgent;

        return $this;
    }
}