<?php

declare(strict_types=1);

namespace App\Component\Captcha\Command;

class ResolveReCaptchaCommand implements ResolveCommandInterface
{
    private $siteKey;
    private $endpoint;

    public function __construct(string $siteKey, string $endpoint)
    {
        $this->siteKey = $siteKey;
        $this->endpoint = $endpoint;
    }

    public function siteKey(): string
    {
        return $this->siteKey;
    }

    public function endpoint(): string
    {
        return $this->endpoint;
    }
}