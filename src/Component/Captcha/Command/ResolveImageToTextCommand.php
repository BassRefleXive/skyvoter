<?php

declare(strict_types = 1);


namespace App\Component\Captcha\Command;


class ResolveImageToTextCommand implements ResolveCommandInterface
{
    private $image;

    public function __construct(string $image)
    {
        $this->image = $image;
    }

    public function image(): string
    {
        return $this->image;
    }
}