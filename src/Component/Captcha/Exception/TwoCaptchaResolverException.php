<?php

declare(strict_types = 1);

namespace App\Component\Captcha\Exception;

class TwoCaptchaResolverException extends CaptchaResolverException
{
}