<?php

declare(strict_types = 1);

namespace App\Component\Captcha\Exception;

class CaptchaResolverException extends \RuntimeException
{
    public static function notImplemented(string $provider, string $method): self
    {
        return new self(
            sprintf(
                'Captcha recognition method "%s" not implemented for provider "%s".',
                $method,
                $provider
            )
        );
    }

    public static function reCaptchaFailed(string $siteKey, string $endpoint, string $message): self
    {
        return new self(
            sprintf(
                'Failed to resolve reCaptcha for site key "%s" on endpoint "%s". Error: %s',
                $siteKey,
                $endpoint,
                $message
            )
        );
    }
}