<?php

declare(strict_types = 1);

namespace App\Component\Captcha\Exception;

class CapMonsterResolverException extends AntiCaptchaResolverException
{
}