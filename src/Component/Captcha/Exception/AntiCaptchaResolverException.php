<?php

declare(strict_types = 1);

namespace App\Component\Captcha\Exception;

class AntiCaptchaResolverException extends CaptchaResolverException
{
    public static function createReCaptchaTaskFailed(string $siteKey, string $endpoint, string $message): self
    {
        return new self(
            sprintf(
                'Failed to create ReCaptcha2 task for site key "%s" on endpoint "%s". Error: %s',
                $siteKey,
                $endpoint,
                $message
            )
        );
    }
    public static function createFunCaptchaTaskFailed(string $publicKey, string $endpoint, string $message): self
    {
        return new self(
            sprintf(
                'Failed to create FunCaptcha task for public key "%s" on endpoint "%s". Error: %s',
                $publicKey,
                $endpoint,
                $message
            )
        );
    }

    public static function createImageToTextTaskFailed(string $message): self
    {
        return new self(sprintf('Failed to create ImageToText task. Error: %s', $message));
    }

    public static function waitResponseFailed(int $taskId): self
    {
        return new self(sprintf('Failed to get response for task %d.', $taskId));
    }

    public static function unexpectedFunCaptchaSolution(): self
    {
        return new self('Unexpected FunCaptcha solution.');
    }
}