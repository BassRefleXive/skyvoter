<?php

declare(strict_types = 1);

namespace App\Component\Captcha;

use App\Component\Captcha\Command\ResolveCommandInterface;
use App\Component\Captcha\Command\ResolveFunCaptchaCommand;
use App\Component\Captcha\Command\ResolveReCaptchaCommand;
use App\Component\Captcha\Exception\CaptchaResolverException;
use App\Component\Captcha\Exception\TwoCaptchaResolverException;
use App\Component\Captcha\Model\ResolverCredentials;
use jumper423\decaptcha\services\RuCaptcha;
use jumper423\decaptcha\services\TwoCaptchaReCaptcha;
use Psr\Log\LoggerInterface;

class TwoCaptchaResolver implements CaptchaResolverInterface
{
    private $resolver;
    private $logger;

    public function __construct(TwoCaptchaReCaptcha $resolver, LoggerInterface $logger)
    {
        $this->resolver = $resolver;
        $this->logger = $logger;
    }

    /**
     * @param ResolverCredentials $credentials
     * @param ResolveReCaptchaCommand $command
     *
     * @return string
     */
    public function resolveReCaptcha(ResolverCredentials $credentials, ResolveCommandInterface $command): string
    {
        $this->logger->info('TwoCaptchaResolver Request', ['site_key' => $command->siteKey(), 'endpoint' => $command->endpoint()]);

        $this->resolver->setParam(RuCaptcha::ACTION_FIELD_KEY, $credentials->apiKey());

        $result = $this->resolver->recognize([
            TwoCaptchaReCaptcha::ACTION_FIELD_GOOGLEKEY => $command->siteKey(),
            TwoCaptchaReCaptcha::ACTION_FIELD_PAGEURL => $command->endpoint(),
        ]);

        if (!$result) {
            $this->logger->error('TwoCaptchaResolver Error', ['message' => $this->resolver->getError(),]);

            throw TwoCaptchaResolverException::reCaptchaFailed($command->siteKey(), $command->endpoint(), $this->resolver->getError());
        }

        $this->logger->info('TwoCaptchaResolver Result', ['captcha' => $this->resolver->getCode(),]);

        return $this->resolver->getCode();
    }

    public function resolveFunCaptcha(ResolverCredentials $credentials, ResolveCommandInterface $command): string
    {
        throw CaptchaResolverException::notImplemented('TwoCaptcha', 'FunCaptcha');
    }

    public function resolveImage(ResolverCredentials $credentials, ResolveCommandInterface $command): string
    {
        throw CaptchaResolverException::notImplemented('TwoCaptcha', 'ResolveImage');
    }
}