<?php

declare(strict_types=1);

namespace App\Component\Captcha;


use App\Component\Captcha\CapMonsterApi\NoCaptcha;
use App\Component\Captcha\Command\ResolveCommandInterface;
use App\Component\Captcha\Command\ResolveReCaptchaCommand;
use App\Component\Captcha\Exception\CapMonsterResolverException;
use App\Component\Captcha\Model\ResolverCredentials;
use Psr\Log\LoggerInterface;

class CapMonsterResolver implements CaptchaResolverInterface
{
    private $noCaptchaResolver;
    private $logger;

    public function __construct(
        NoCaptcha $noCaptchaResolver,
        LoggerInterface $logger
    )
    {
        $this->noCaptchaResolver = $noCaptchaResolver;
        $this->logger = $logger;
    }

    /**
     * @param ResolverCredentials $credentials
     * @param ResolveReCaptchaCommand $command
     *
     * @return string
     */
    public function resolveReCaptcha(ResolverCredentials $credentials, ResolveCommandInterface $command): string
    {
        $this->logger->info('CapMonster Request', ['site_key' => $command->siteKey(), 'endpoint' => $command->endpoint()]);

        $this->noCaptchaResolver->setKey($credentials->apiKey());
        $this->noCaptchaResolver->setWebsiteURL($command->endpoint());
        $this->noCaptchaResolver->setWebsiteKey($command->siteKey());

        if (null === $taskId = $this->noCaptchaResolver->createTask()) {
            throw CapMonsterResolverException::createReCaptchaTaskFailed($command->siteKey(), $command->endpoint(), $this->noCaptchaResolver->getErrorMessage());
        }

        if (!$this->noCaptchaResolver->waitForResult($taskId)) {
            throw CapMonsterResolverException::waitResponseFailed($taskId);
        }

        return $this->noCaptchaResolver->getTaskSolution();
    }

    public function resolveFunCaptcha(ResolverCredentials $credentials, ResolveCommandInterface $command): string
    {
        throw CapMonsterResolverException::notImplemented('CapMonster', 'FunCaptcha');
    }

    public function resolveImage(ResolverCredentials $credentials, ResolveCommandInterface $command): string
    {
        throw CapMonsterResolverException::notImplemented('CapMonster', 'ResolveImage');
    }
}