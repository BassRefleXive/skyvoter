<?php

declare(strict_types = 1);

namespace App\Component\Captcha;

use App\Component\Captcha\Command\ResolveCommandInterface;
use App\Component\Captcha\Model\ResolverCredentials;

interface CaptchaResolverInterface
{
    public function resolveReCaptcha(ResolverCredentials $credentials, ResolveCommandInterface $command): string;

    public function resolveFunCaptcha(ResolverCredentials $credentials, ResolveCommandInterface $command): string;

    public function resolveImage(ResolverCredentials $credentials, ResolveCommandInterface $command): string;
}