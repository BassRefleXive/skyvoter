<?php

declare(strict_types = 1);

namespace App\Component\Captcha\Enum;

use MabeEnum\Enum;

final class Resolver extends Enum
{
    public const TWO_CAPTCHA = 'two_captcha';
    public const ANTI_CAPTCHA = 'anti_captcha';
    public const CAP_MONSTER = 'cap_monster';
}