<?php

declare(strict_types = 1);


namespace App\Component\Captcha\Enum;


use MabeEnum\Enum;

final class CaptchaType extends Enum
{
    public const RE_CAPTCHA_2 = 're_captcha_2';
    public const FUN_CAPTCHA = 'fun_captcha';
    public const IMAGE_TO_TEXT = 'image_to_text';

    public function isReCaptcha2(): bool
    {
        return $this->is(self::RE_CAPTCHA_2);
    }

    public function isFunCaptcha(): bool
    {
        return $this->is(self::FUN_CAPTCHA);
    }

    public function isImageTotext(): bool
    {
        return $this->is(self::IMAGE_TO_TEXT);
    }
}