<?php

declare(strict_types = 1);

namespace App\Component\Tor\Guzzle\Middleware;

use App\Component\Tor\ProxyChanger;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Promise\PromiseInterface;
use Psr\Http\Message\RequestInterface;

class ProxyMiddleware
{
    private $proxyChanger;
    private $proxy;

    public function __construct(ProxyChanger $proxyChanger, string $proxy)
    {
        $this->proxyChanger = $proxyChanger;
        $this->proxy = $proxy;
    }

    public function __invoke(callable $handler): callable
    {
        return function (RequestInterface $request, array $options) use ($handler): PromiseInterface {
            $options = array_replace_recursive([
                'proxy' => $this->proxy,
                'curl'  => [
                    CURLOPT_PROXYTYPE    => CURLPROXY_SOCKS5_HOSTNAME,
                    CURLOPT_FORBID_REUSE => true,
                ],
            ], $options);
            if (array_key_exists('tor_new_identity', $options) && $options['tor_new_identity']) {
                try {
                    $this->proxyChanger->change((float) (@$options['tor_new_identity_timeout'] ?: 3));
                } catch (GuzzleException $e) {
                    if (@$options['tor_new_identity_exception']) {
                        throw $e;
                    }
                }
            }

            return $handler($request, $options);
        };
    }
}