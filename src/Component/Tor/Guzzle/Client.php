<?php

declare(strict_types = 1);

namespace App\Component\Tor\Guzzle;

use GuzzleHttp\Client as GuzzleClient;

class Client extends GuzzleClient
{
}