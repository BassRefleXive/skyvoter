<?php

declare(strict_types = 1);

namespace App\Component\Tor;

use GuzzleHttp\HandlerStack as GuzzleHandlerStack;

class HandlerStack extends GuzzleHandlerStack
{

}