<?php

declare(strict_types = 1);

namespace App\Component\Tor;

use App\Component\IpLookup\IpDiscoverer;
use App\Component\Proxy\Model\Proxy;
use Psr\Log\LoggerInterface;

class ProxyChanger
{
    private const TOR_OK = 250;
    public const CHANGE_PROXY_SLEEP_TIME = 0;
    public const CHANGE_PROXY_TIMEOUT = 3.;

    private $ipDiscoverer;
    private $controlEndpoint;
    private $proxyEndpoint;
    private $password;
    private $logger;

    public function __construct(IpDiscoverer $ipDiscoverer, string $controlEndpoint, string $password, string $proxyEndpoint, LoggerInterface $logger)
    {
        $this->ipDiscoverer = $ipDiscoverer;
        $this->controlEndpoint = $controlEndpoint;
        $this->proxyEndpoint = $proxyEndpoint;
        $this->password = $password;
        $this->logger = $logger;
    }

    public function change(float $timeout = self::CHANGE_PROXY_TIMEOUT): string
    {
        list($proxyIp, $proxyPort) = explode(':', $this->proxyEndpoint);
        $currentIp = $this->ipDiscoverer->discover(new Proxy($proxyIp, (int) $proxyPort));

        list($ip, $port) = explode(':', $this->controlEndpoint);
        $socket = @fsockopen($ip, (int) $port, $errNo, $errStr, (float) $timeout);
        if (!$socket) {
            throw new \RuntimeException("Could not connect to Tor client on $this->controlEndpoint: $errNo $errStr");
        }

        fputs($socket, sprintf("AUTHENTICATE \"%s\"\r\n", $this->password));
        $response = fread($socket, 1024);
        $code = explode(' ', $response, 2)[0];

        if (self::TOR_OK != $code) {
            throw new \RuntimeException("Could not authenticate on Tor client, response: $response");
        }

        fputs($socket, "SIGNAL NEWNYM\r\n");
        $response = fread($socket, 1024);
        $code = explode(' ', $response, 2)[0];

        if (self::TOR_OK != $code) {
            throw new \RuntimeException("Could not get new identity, response: $response");
        }

        fclose($socket);

        do {
            sleep(1);

            $newIp = $this->ipDiscoverer->discover(new Proxy($proxyIp, (int) $proxyPort));
        } while ($newIp === $currentIp);

        $this->logger->info('Changed TOR ip from {from} to {to}.', [
            'from' => $currentIp,
            'to' => $newIp,
        ]);

        return $newIp;
    }
}