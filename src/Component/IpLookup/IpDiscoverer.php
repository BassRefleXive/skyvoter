<?php

declare(strict_types = 1);

namespace App\Component\IpLookup;

use App\Component\Proxy\Model\Proxy;

class IpDiscoverer
{
    private const ENDPOINT = 'https://api.ipify.org?format=json';

    public function discover(Proxy $proxy): string
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::ENDPOINT);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5_HOSTNAME);
        curl_setopt($ch, CURLOPT_PROXY, sprintf('%s:%d', $proxy->ip(), $proxy->port()));

        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);

        $result = curl_exec($ch);
        $curlError = curl_error($ch);

        if ($curlError != "") {
            return $curlError;
        }
        curl_close($ch);

        return json_decode($result, true)['ip'] ?? '';
    }
}