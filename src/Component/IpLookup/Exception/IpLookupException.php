<?php

declare(strict_types = 1);

namespace App\Component\IpLookup\Exception;

class IpLookupException extends \RuntimeException
{
    public static function invalidResponse(string $response): self
    {
        return new self(
            sprintf(
                'Invalid response received. Response: "%s".',
                $response
            )
        );
    }

    public static function missingData(): self
    {
        return new self('Missing IP address in response.');
    }
}