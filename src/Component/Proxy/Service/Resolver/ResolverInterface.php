<?php

declare(strict_types = 1);


namespace App\Component\Proxy\Service\Resolver;


use App\Component\Proxy\Model\Proxy;

interface ResolverInterface
{
    public function setNext(self $next): void;

    public function next(): Proxy;
}