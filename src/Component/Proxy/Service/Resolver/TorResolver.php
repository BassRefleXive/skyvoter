<?php

declare(strict_types = 1);

namespace App\Component\Proxy\Service\Resolver;


use App\Component\DirectorConnector\JobService;
use App\Component\IpLookup\IpDiscoverer;
use App\Component\Proxy\Enum\ProxyResolver;
use App\Component\Proxy\Model\Proxy;
use App\Component\Tor\ProxyChanger;
use Psr\Log\LoggerInterface;

class TorResolver extends AbstractResolver implements ResolverInterface
{
    private const TRIES_COUNT = 3;
    private const SKIPS_COUNT = 4;

    private $proxyChanger;
    private $jobService;
    private $logger;
    private $ip;
    private $port;
    private $skippedTimes;

    public function __construct(
        ProxyChanger $proxyChanger,
        JobService $jobService,
        LoggerInterface $logger,
        string $ip,
        int $port
    )
    {
        $this->proxyChanger = $proxyChanger;
        $this->jobService = $jobService;
        $this->logger = $logger;
        $this->ip = $ip;
        $this->port = $port;
        $this->skippedTimes = 0;
    }

    public function resolveNext(): ?Proxy
    {
        if (0 !== $this->skippedTimes && self::SKIPS_COUNT > $this->skippedTimes) {
            $this->skippedTimes++;
            $this->logger->warning('Skipping TOR resolver {i} of {count}.', [
                'i'     => $this->skippedTimes,
                'count' => self::SKIPS_COUNT,
            ]);

            return null;
        }

        $res = $this->findUnused();

        return $res
            ? (new Proxy($this->ip, $this->port))
                ->specifyResolver(ProxyResolver::byValue(ProxyResolver::TOR))
            : null;
    }

    private function findUnused(): bool
    {
        $this->skippedTimes = 0;
        $try = 0;

        do {
            $ip = $this->proxyChanger->change();

            $isUsedBefore = false;
//            $isUsedBefore = $this->jobService->checkIp($ip);

            if (!$isUsedBefore) {
                $this->logger->info('Discovered unused tor ip {ip}. Using it.', [
                    'ip' => $ip,
                ]);

                return true;
            }

            $this->logger->warning('IP {ip} has been used already.', ['ip' => $ip]);

            $try++;
        } while ($try <= self::TRIES_COUNT);

        $this->logger->warning('Unsuccessfully tried to resolve tor ip {count} times. Giving up.', ['count' => self::TRIES_COUNT]);
        $this->skippedTimes++;

        return false;
    }
}