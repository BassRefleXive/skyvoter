<?php

declare(strict_types = 1);


namespace App\Component\Proxy\Service\Resolver;


use App\Component\Proxy\Enum\ProxyResolver;
use App\Component\Proxy\Model\Proxy;
use Psr\Log\LoggerInterface;

class RotatingResolver extends AbstractResolver implements ResolverInterface
{
    private $ip;
    private $port;
    private $logger;

    public function __construct(LoggerInterface $logger, string $ip, int $port)
    {
        $this->ip = $ip;
        $this->port = $port;
        $this->logger = $logger;
    }

    public function resolveNext(): Proxy
    {
        $this->logger->warning('Using RotatingResolver {ip}:{port}.', [
            'ip'   => $this->ip,
            'port' => $this->port,
        ]);

        return (new Proxy($this->ip, $this->port))
            ->specifyResolver(ProxyResolver::byValue(ProxyResolver::ROTATING));
    }
}