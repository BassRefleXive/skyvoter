<?php

declare(strict_types = 1);

namespace App\Component\Proxy\Service\Resolver;

use App\Component\Core\Http\Criteria\RequestCriteria;
use App\Component\Core\Http\HttpClientInterface;
use App\Component\Proxy\Assembler\RequestAssembler;
use App\Component\Proxy\Criteria\ProxyIssuerUriCriteria;
use App\Component\Proxy\Enum\ProxyResolver;
use App\Component\Proxy\Factory\ProxyFactory;
use App\Component\Proxy\Model\Proxy;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;

class ProxyListResolver extends AbstractResolver implements ResolverInterface
{
    private const MAX_CONTINUOUS_USAGES = 3;
    private const PAUSE_BEFORE_NEXT_TRY = 3;

    private $client;
    private $requestAssembler;
    private $logger;
    private $proxyFactory;
    private $usagesCount;

    public function __construct(HttpClientInterface $client, RequestAssembler $requestAssembler, LoggerInterface $logger)
    {
        $this->client = $client;
        $this->requestAssembler = $requestAssembler;
        $this->logger = $logger;
        $this->proxyFactory = new ProxyFactory();
        $this->usagesCount = 0;
    }

    public function resolveNext(): ?Proxy
    {
        if ($this->usagesCount > self::MAX_CONTINUOUS_USAGES + self::PAUSE_BEFORE_NEXT_TRY) {
            $this->usagesCount = 0;
        }

        if ($this->usagesCount > self::MAX_CONTINUOUS_USAGES) {
            $this->logger->warning('Trying to use ProxyList resolver {usagesCount} of {allowedUsages} usages in a row. Giving a try to next resolver {try} of {total}.', [
                'usagesCount'   => $this->usagesCount,
                'allowedUsages' => self::MAX_CONTINUOUS_USAGES,
                'try'           => $this->usagesCount - self::MAX_CONTINUOUS_USAGES,
                'total'         => self::PAUSE_BEFORE_NEXT_TRY,
            ]);

            $this->usagesCount++;
            return null;
        }

        $requestCriteria = new RequestCriteria(
            ProxyIssuerUriCriteria::next(),
            Request::METHOD_GET
        );

        try {
            $proxy = $this->proxyFactory
                ->fromResponse(
                    $this->client->send($this->requestAssembler->fromCriteria($requestCriteria))
                )
                ->specifyResolver(ProxyResolver::byValue(ProxyResolver::PROXY_LIST));
        } catch (\Throwable $e) {
            $this->logger->info('Failed to resolve proxy ProxyList proxy. Error "{error}."', [
                'error'   => $e->getMessage(),
            ]);

            return null;
        }

        $this->logger->info('Using Proxy {ip}:{port} from ProxyList resolver.', [
            'ip'   => $proxy->ip(),
            'port' => $proxy->port(),
        ]);

        $this->usagesCount++;

        return $proxy;
    }
}