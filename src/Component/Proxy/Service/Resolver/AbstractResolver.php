<?php

declare(strict_types = 1);


namespace App\Component\Proxy\Service\Resolver;


use App\Component\Proxy\Exception\ProxyIssuerException;
use App\Component\Proxy\Model\Proxy;

abstract class AbstractResolver implements ResolverInterface
{
    /**
     * @var ResolverInterface
     */
    protected $next;

    public final function setNext(ResolverInterface $next): void
    {
        $this->next = $next;
    }

    public final function next(): Proxy
    {
        $next = $this->resolveNext();

        if (null !== $next) {
            return $next;
        }

        if (null === $next && null === $this->next) {
            throw ProxyIssuerException::failedToResolve();
        }

        return $this->next->next();
    }

    abstract public function resolveNext(): ?Proxy;
}