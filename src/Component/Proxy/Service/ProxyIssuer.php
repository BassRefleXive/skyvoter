<?php

declare(strict_types = 1);

namespace App\Component\Proxy\Service;


use App\Component\Proxy\Enum\ProxyResolver;
use App\Component\Proxy\Exception\ProxyIssuerException;
use App\Component\Proxy\Model\Proxy;
use App\Component\Proxy\Service\Resolver\ResolverInterface;
use App\Component\Tor\ProxyChanger;

class ProxyIssuer
{
    /**
     * @var ResolverInterface|null
     */
    protected $first = null;

    private $proxy;
    private $proxyChanger;

    public function __construct(ProxyChanger $proxyChanger, array $resolvers)
    {
        $this->proxyChanger = $proxyChanger;

        $current = null;

        foreach ($resolvers as $resolver) {
            if (is_null($this->first)) {
                $this->first = $resolver;
            }

            if (!is_null($current)) {
                $current->setNext($resolver);
            }

            $current = $resolver;
        }
    }

    public function next(): Proxy
    {
        if ($this->first) {
            $this->proxy = $this->first->next();

            if (!$this->proxy->resolvedBy()->isTor()) {
                $this->proxyChanger->change();
            }

            return $this->proxy;
        }

        throw ProxyIssuerException::noResolverDefined();
    }

    public function current(): Proxy
    {
        if (null === $this->proxy) {
            return $this->next();
        }

        return $this->proxy;
    }
}