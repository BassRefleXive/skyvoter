<?php

declare(strict_types=1);

namespace App\Component\Proxy\Assembler;

use App\Component\Core\Http\Assembler\RequestAssembler as BaseRequestAssembler;

class RequestAssembler extends BaseRequestAssembler
{

}