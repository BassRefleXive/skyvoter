<?php

declare(strict_types=1);

namespace App\Component\Proxy\GuzzleHttp;

use App\Component\Core\Http\GuzzleHttp\Client as BaseClient;

class ProxyLessClient extends BaseClient
{
}