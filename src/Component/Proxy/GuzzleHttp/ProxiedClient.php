<?php

declare(strict_types=1);

namespace App\Component\Proxy\GuzzleHttp;


use GuzzleHttp\Client;

class ProxiedClient extends Client
{

}