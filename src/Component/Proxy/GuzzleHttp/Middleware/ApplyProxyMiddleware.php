<?php

declare(strict_types=1);

namespace App\Component\Proxy\GuzzleHttp\Middleware;


use App\Component\Proxy\Service\ProxyIssuer;
use GuzzleHttp\Promise\PromiseInterface;
use Psr\Http\Message\RequestInterface;

class ApplyProxyMiddleware
{
    private $issuer;

    public function __construct(ProxyIssuer $issuer)
    {
        $this->issuer = $issuer;
    }

    public function __invoke(callable $handler): callable
    {
        return function (RequestInterface $request, array $options) use ($handler): PromiseInterface {
            $options = array_replace_recursive([
                'proxy' => $this->issuer->current()->http(),
            ], $options);

            return $handler($request, $options);
        };
    }
}