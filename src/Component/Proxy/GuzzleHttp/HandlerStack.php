<?php

declare(strict_types = 1);

namespace App\Component\Proxy\GuzzleHttp;

use GuzzleHttp\HandlerStack as GuzzleHandlerStack;

class HandlerStack extends GuzzleHandlerStack
{

}