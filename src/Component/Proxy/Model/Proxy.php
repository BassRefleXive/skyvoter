<?php

declare(strict_types = 1);

namespace App\Component\Proxy\Model;


use App\Component\Proxy\Enum\ProxyResolver;
use App\Component\Proxy\Enum\ProxyType;

class Proxy
{
    private $ip;
    private $port;
    private $type;
    private $resolvedBy;

    public function __construct(string $ip, int $port, string $type = ProxyType::HTTPS)
    {
        $this->ip = $ip;
        $this->port = $port;
        $this->type = ProxyType::byValue($type);
    }

    public function ip(): string
    {
        return $this->ip;
    }

    public function port(): int
    {
        return $this->port;
    }

    public function type(): ProxyType
    {
        return $this->type;
    }

    public function specifyResolver(ProxyResolver $resolver): self
    {
        $this->resolvedBy = $resolver;

        return $this;
    }

    public function resolvedBy(): ProxyResolver
    {
        return $this->resolvedBy;
    }

    public function http(): string
    {
        return sprintf('http://%s:%d', $this->ip, $this->port);
    }
}