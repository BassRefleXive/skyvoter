<?php

declare(strict_types = 1);


namespace App\Component\Proxy\Enum;


use MabeEnum\Enum;

final class ProxyResolver extends Enum
{
    public const TOR = 'tor';
    public const PROXY_LIST = 'proxy_list';
    public const ROTATING = 'rotating';

    public function isTor(): bool
    {
        return $this->is(self::TOR);
    }

    public function isProxyList(): bool
    {
        return $this->is(self::PROXY_LIST);
    }

    public function isRotating(): bool
    {
        return $this->is(self::ROTATING);
    }
}