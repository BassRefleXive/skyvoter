<?php

declare(strict_types = 1);

namespace App\Component\Proxy\Enum;

use MabeEnum\Enum;

final class ProxyType extends Enum
{
    public const HTTP = 'http';
    public const HTTPS = 'https';
    public const SOCKS5 = 'socks5';
}