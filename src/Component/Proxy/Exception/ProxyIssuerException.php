<?php

declare(strict_types = 1);


namespace App\Component\Proxy\Exception;


class ProxyIssuerException extends \RuntimeException
{
    public static final function noResolverDefined(): self
    {
        return new self('No proxy resolvers defined.');
    }

    public static final function failedToResolve(): self
    {
        return new self('All resolvers cannot determine proxy for requests.');
    }
}