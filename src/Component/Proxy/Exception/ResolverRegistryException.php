<?php

declare(strict_types = 1);


namespace App\Component\Proxy\Exception;


class ResolverRegistryException extends \LogicException
{
    public static function alreadyExists(string $key): self
    {
        return new self(sprintf('Resolver with key "%s" already exists in registry.', $key));
    }

    public static function notFound(string $key): self
    {
        return new self(sprintf('Resolver with key "%s" not fount in registry.', $key));
    }
}