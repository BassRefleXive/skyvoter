<?php

declare(strict_types=1);

namespace App\Component\Proxy\Factory;


use App\Component\Core\Http\Exception\ResponseException;
use App\Component\Proxy\Model\Proxy;
use Psr\Http\Message\ResponseInterface;

class ProxyFactory
{
    public function fromResponse(ResponseInterface $response): Proxy
    {
        $contents = $response->getBody()->getContents();

        if (null === $responseData = json_decode($contents, true)) {
            throw ResponseException::invalidFormat('json', $contents);
        }

        return self::fromArray($responseData);
    }

    public function fromArray(array $data): Proxy
    {
        self::checkValueExistence($data, ['ip', 'port']);

        return new Proxy($data['ip'], (int) $data['port']);
    }

    private static function checkValueExistence(array $array, array $indexes)
    {
        array_map(function (string $index) use ($array) {
            if (!isset($array[$index])) {
                throw ResponseException::missingIndex($index);
            }
        }, $indexes);
    }
}