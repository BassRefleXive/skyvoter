<?php

declare(strict_types = 1);

namespace App\Component\DirectorConnector\Factory;

use App\Component\Captcha\Enum\Resolver;
use App\Component\DirectorConnector\Model\CaptchaResolver;

class CaptchaResolverFactory
{
    public function fromResponseArray(array $data): CaptchaResolver
    {
        return new CaptchaResolver(
            Resolver::byValue($data['code']),
            $data['key']
        );
    }
}