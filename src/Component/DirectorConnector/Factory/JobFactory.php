<?php

declare(strict_types = 1);

namespace App\Component\DirectorConnector\Factory;

use App\Component\DirectorConnector\Model\Job;
use App\Component\TopProcessor\Enum\Top;
use Ramsey\Uuid\Uuid;

class JobFactory
{
    public function fromResponseArray(array $data): Job
    {
        return new Job(
            Uuid::fromString($data['id']),
            Top::byValue($data['server']['top']['code']),
            (new CaptchaResolverFactory())->fromResponseArray($data['server']['top']['captcha_resolver']),
            $data['server']['provider_id']
        );
    }
}