<?php

declare(strict_types = 1);

namespace App\Component\DirectorConnector;

use App\Component\DirectorConnector\Exception\ResponseException;
use App\Component\DirectorConnector\Factory\JobFactory;
use App\Component\DirectorConnector\Model\Job;
use App\Component\DirectorConnector\Model\JobsCollection;
use App\Component\DirectorConnector\Model\ProxyType;
use App\Component\Proxy\Enum\ProxyResolver;
use GuzzleHttp\ClientInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;

class JobService
{
    private $client;
    private $logger;

    public function __construct(ClientInterface $client, LoggerInterface $logger)
    {
        $this->client = $client;
        $this->logger = $logger;
    }

    public function next(): JobsCollection
    {
        $response = $this->client->request('GET', '/worker/job/next');

        return new JobsCollection(array_map([new JobFactory(), 'fromResponseArray'], $this->arrayResponse($response)));
    }

    public function proxyType(): ProxyType
    {
        $response = $this->client->request('GET', '/worker/proxy/type');

        $data = $this->arrayResponse($response);

        return new ProxyType(
            Uuid::fromString($data['id']),
            ProxyResolver::byValue($data['code'])
        );
    }

    public function execute(Job $job): void
    {
        $this->logger->info('Reporting executed job {id} on server {server} in top {top}.', [
            'id' => $job->id(),
            'server' => $job->serverId(),
            'top' => $job->top()->getName(),
        ]);

        $this->client->request('PUT', sprintf('/worker/job/%s/execute', $job->id()));
    }

    public function fail(Job $job): void
    {
        $this->logger->info('Reporting failed job {id} on server {server} in top {top}.', [
            'id' => $job->id(),
            'server' => $job->serverId(),
            'top' => $job->top()->getName(),
        ]);

        $this->client->request('PUT', sprintf('/worker/job/%s/fail', $job->id()));
    }

    public function checkIp(string $ip): bool
    {
        $response = $this->client->request('GET', sprintf('/worker/ip-address/check?address=%s', $ip));

        return $this->arrayResponse($response)['used'];
    }

    private function arrayResponse(ResponseInterface $response): array
    {
        if (null === $data = json_decode($response->getBody()->getContents(), true)) {
            throw ResponseException::invalidFormat();
        }

        return $data;
    }
}