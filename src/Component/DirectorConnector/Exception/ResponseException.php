<?php

declare(strict_types = 1);

namespace App\Component\DirectorConnector\Exception;

class ResponseException extends \RuntimeException
{
    public static function invalidFormat(): self
    {
        return new self('Invalid response format. json expected.');
    }
}