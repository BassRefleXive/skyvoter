<?php

declare(strict_types = 1);

namespace App\Component\DirectorConnector\Model;

use App\Component\TopProcessor\Enum\Top;
use Ramsey\Uuid\UuidInterface;

class Job
{
    private $id;
    private $top;
    private $captchaResolver;
    private $serverId;

    public function __construct(UuidInterface $id, Top $top, CaptchaResolver $captchaResolver, string $serverId)
    {
        $this->id = $id;
        $this->top = $top;
        $this->captchaResolver = $captchaResolver;
        $this->serverId = $serverId;
    }

    public function id(): UuidInterface
    {
        return $this->id;
    }

    public function top(): Top
    {
        return $this->top;
    }

    public function captchaResolver(): CaptchaResolver
    {
        return $this->captchaResolver;
    }

    public function serverId(): string
    {
        return $this->serverId;
    }
}