<?php

declare(strict_types = 1);


namespace App\Component\DirectorConnector\Model;


use App\Component\Proxy\Enum\ProxyResolver;
use Ramsey\Uuid\UuidInterface;

class ProxyType
{
    private $id;
    private $resolver;

    public function __construct(UuidInterface $id, ProxyResolver $resolver)
    {
        $this->id = $id;
        $this->resolver = $resolver;
    }

    public function id(): UuidInterface
    {
        return $this->id;
    }

    public function resolver(): ProxyResolver
    {
        return $this->resolver;
    }
}