<?php

declare(strict_types = 1);

namespace App\Component\DirectorConnector\Model;

use App\Component\Captcha\Enum\Resolver;

class CaptchaResolver
{
    private $resolver;
    private $key;

    public function __construct(Resolver $resolver, string $key)
    {
        $this->resolver = $resolver;
        $this->key = $key;
    }

    public function resolver(): Resolver
    {
        return $this->resolver;
    }

    public function key(): string
    {
        return $this->key;
    }
}