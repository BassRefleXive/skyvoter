<?php

declare(strict_types = 1);


namespace App\Component\DirectorConnector\Model;


use Symfony\Component\Console\Exception\LogicException;

class JobsCollection
{
    private $items = [];

    public function __construct(array $items = [])
    {
        foreach ($items as $item) {
            $this->add($item);
        }
    }

    public function add(Job $job): void
    {
        $this->items[] = $job;
    }

    public function map(callable $fn): self
    {
        $newElements = [];

        foreach ($this->items as $i => $element) {
            $result = $fn($element);

            if (!$result instanceof Job) {
                throw new LogicException(sprintf('Callable result must be instance of "%s", instance of "%s" occurred.', Job::class, get_class($result)));
            }

            $newElements[$i] = $result;
        }

        return $this->createNew($newElements);
    }

    public function all(): array
    {
        return $this->items;
    }

    public function count(): int
    {
        return count($this->items);
    }

    protected function createNew($items)
    {
        return new static($items);
    }
}