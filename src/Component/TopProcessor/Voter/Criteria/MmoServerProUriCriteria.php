<?php

declare(strict_types=1);

namespace App\Component\TopProcessor\Voter\Criteria;


class MmoServerProUriCriteria extends VoteUriCriteria
{
    public static function domain(): string
    {
        return 'http://mmoserver.pro';
    }

    public static function landing(string $id): self
    {
        return new self(
            'topsite/mu-online/in/id{id}',
            [
                'id' => $id,
            ]
        );
    }

    public static function vote(string $id): self
    {
        return new self(
            'topsite/mu-online/in/id{id}',
            [
                'id' => $id,
            ]
        );
    }
}