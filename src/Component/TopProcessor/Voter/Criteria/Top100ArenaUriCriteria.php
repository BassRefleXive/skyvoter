<?php

declare(strict_types=1);

namespace App\Component\TopProcessor\Voter\Criteria;


class Top100ArenaUriCriteria extends VoteUriCriteria
{
    public static function domain(): string
    {
        return 'http://www.top100arena.com';
    }

    public static function landing(string $id): self
    {
        return new self(
            'in.asp',
            [],
            [
                'id' => $id,
            ]
        );
    }

    public static function vote(string $id): self
    {
        return new self(
            'in.asp',
            [],
            [
                'id' => $id,
            ]
        );
    }
}