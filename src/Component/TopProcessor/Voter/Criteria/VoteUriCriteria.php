<?php

declare(strict_types=1);

namespace App\Component\TopProcessor\Voter\Criteria;


use App\Component\Core\Http\Criteria\UriCriteria;

abstract class VoteUriCriteria extends UriCriteria
{
    abstract public static function domain(): string;

    abstract public static function landing(string $id);

    abstract public static function vote(string $id);
}