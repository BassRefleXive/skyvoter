<?php

declare(strict_types=1);

namespace App\Component\TopProcessor\Voter\Criteria;


class GamesTop100UriCriteria extends VoteUriCriteria
{
    public static function domain(): string
    {
        return 'http://gamestop100.com';
    }

    public static function landing(string $id): self
    {
        return new self(
            'vote_for_site',
            [],
            [
                'id' => $id,
            ]
        );
    }

    public static function vote(string $id): self
    {
        return new self('vote_for_site/vote');
    }
}