<?php

declare(strict_types=1);

namespace App\Component\TopProcessor\Voter\Criteria;


class TopServers200UriCriteria extends VoteUriCriteria
{
    public static function domain(): string
    {
        return 'http://www.topservers200.com';
    }

    public static function landing(string $id): self
    {
        return new self(
            'in.php',
            [],
            [
                'id' => $id,
            ]
        );
    }

    public static function vote(string $id): self
    {
        return new self(
            'in.php',
            [],
            [
                'id' => $id,
            ]
        );
    }
}