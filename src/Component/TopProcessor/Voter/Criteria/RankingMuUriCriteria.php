<?php

declare(strict_types = 1);

namespace App\Component\TopProcessor\Voter\Criteria;


class RankingMuUriCriteria extends VoteUriCriteria
{
    public static function domain(): string
    {
        return 'http://rankingmu.com';
    }

    public static function landing(string $id): self
    {
        return new self(
            'in.php',
            [],
            [
                'wid' => $id,
            ]
        );
    }

    public static function vote(string $id): self
    {
        return new self(
            'in.php',
            [],
            [
                'accion' => 'votando',
            ]
        );
    }
}