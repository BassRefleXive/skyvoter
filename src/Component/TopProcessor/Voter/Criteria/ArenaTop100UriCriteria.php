<?php

declare(strict_types = 1);

namespace App\Component\TopProcessor\Voter\Criteria;


use App\Component\TopProcessor\Exception\UriException;

class ArenaTop100UriCriteria extends VoteUriCriteria
{
    public static function domain(): string
    {
        return 'http://www.arena-top100.com';
    }

    public static function landing(string $id): self
    {
        return new self(
            'index.php',
            [],
            [
                'a' => 'in',
                'u' => $id,
            ]
        );
    }

    public static function vote(string $id): self
    {
        throw UriException::notImplemented();
    }
}