<?php

declare(strict_types = 1);

namespace App\Component\TopProcessor\Voter\Criteria;


use App\Component\TopProcessor\Exception\UriException;

class TopMmoRpgServersUriCriteria extends VoteUriCriteria
{
    public static function domain(): string
    {
        return 'http://www.topmmorpgservers.com';
    }

    public static function landing(string $id): self
    {
        return new self(
            'in/{id}',
            [
                'id' => $id,
            ]
        );
    }

    public static function vote(string $id): self
    {
        throw UriException::notImplemented();
    }
}