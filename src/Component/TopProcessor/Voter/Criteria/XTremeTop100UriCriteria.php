<?php

declare(strict_types=1);

namespace App\Component\TopProcessor\Voter\Criteria;


class XTremeTop100UriCriteria extends VoteUriCriteria
{
    public static function domain(): string
    {
        return 'http://www.xtremetop100.com';
    }

    public static function landing(string $id): self
    {
        return new self(
            'in.php',
            [],
            [
                'site' => $id,
            ]
        );
    }

    public static function vote(string $id): self
    {
        return new self(
            'in-post.php',
            [],
            [
                'site' => $id,
            ]
        );
    }
}