<?php

declare(strict_types=1);

namespace App\Component\TopProcessor\Voter\Criteria;


class ETopGamesUriCriteria extends VoteUriCriteria
{
    public static function domain(): string
    {
        return 'http://www.etopgames.com';
    }

    public static function landing(string $id): self
    {
        return new self(
            'index.php',
            [],
            [
                'a' => 'in',
                'u' => $id,
            ]
        );
    }

    public static function vote(string $id): self
    {
        return new self(
            'index.php',
            [],
            [
                'a' => 'in',
                'u' => $id,
            ]
        );
    }
}