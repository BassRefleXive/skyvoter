<?php

declare(strict_types = 1);

namespace App\Component\TopProcessor\Voter\Criteria;


use App\Component\TopProcessor\Exception\UriException;

class MmoTop100UriCriteria extends VoteUriCriteria
{
    public static function domain(): string
    {
        return 'http://www.mmotop100.com';
    }

    public static function landing(string $id): self
    {
        return new self(
            '/vote/{id}',
            [
                'id' => $id,
            ]
        );
    }

    public static function vote(string $id): self
    {
        throw UriException::notImplemented();
    }
}