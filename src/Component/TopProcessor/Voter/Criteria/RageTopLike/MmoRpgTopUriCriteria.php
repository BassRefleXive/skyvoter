<?php

declare(strict_types=1);

namespace App\Component\TopProcessor\Voter\Criteria\RageTopLike;


class MmoRpgTopUriCriteria extends AbstractUriCriteria
{
    public static function domain(): string
    {
        return 'http://mmorpg-top.com';
    }
}