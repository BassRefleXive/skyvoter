<?php

declare(strict_types=1);

namespace App\Component\TopProcessor\Voter\Criteria\RageTopLike;


use App\Component\TopProcessor\Voter\Criteria\VoteUriCriteria;

abstract class AbstractUriCriteria extends VoteUriCriteria
{
    public static function landing(string $id): self
    {
        return new static(
            'index.php',
            [],
            [
                'do' => 'votes',
                'id' => $id,
            ]
        );
    }

    public static function vote(string $id): self
    {
        return new static(
            'index.php',
            [],
            [
                'do' => 'votes',
                'id' => $id,
            ]
        );
    }
}