<?php

declare(strict_types=1);

namespace App\Component\TopProcessor\Voter\Criteria\RageTopLike;


class MmoRpg100UriCriteria extends AbstractUriCriteria
{
    public static function domain(): string
    {
        return 'http://mmorpg-100.com';
    }
}