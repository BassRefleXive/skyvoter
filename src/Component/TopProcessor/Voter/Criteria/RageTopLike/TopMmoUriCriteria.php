<?php

declare(strict_types=1);

namespace App\Component\TopProcessor\Voter\Criteria\RageTopLike;


class TopMmoUriCriteria extends AbstractUriCriteria
{
    public static function domain(): string
    {
        return 'http://top-mmo.com';
    }
}