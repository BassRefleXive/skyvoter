<?php

declare(strict_types=1);

namespace App\Component\TopProcessor\Voter\Criteria\RageTopLike;


class RageTopUriCriteria extends AbstractUriCriteria
{
    public static function domain(): string
    {
        return 'http://ragetop.com';
    }
}