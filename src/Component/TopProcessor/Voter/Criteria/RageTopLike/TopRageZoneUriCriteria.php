<?php

declare(strict_types=1);

namespace App\Component\TopProcessor\Voter\Criteria\RageTopLike;


class TopRageZoneUriCriteria extends AbstractUriCriteria
{
    public static function domain(): string
    {
        return 'http://topragezone.com';
    }
}