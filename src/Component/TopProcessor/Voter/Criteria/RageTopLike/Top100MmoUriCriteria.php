<?php

declare(strict_types=1);

namespace App\Component\TopProcessor\Voter\Criteria\RageTopLike;


class Top100MmoUriCriteria extends AbstractUriCriteria
{
    public static function domain(): string
    {
        return 'http://top100mmo.com';
    }
}