<?php

declare(strict_types=1);

namespace App\Component\TopProcessor\Voter\Criteria\RageTopLike;


class Top200MmoUriCriteria extends AbstractUriCriteria
{
    public static function domain(): string
    {
        return 'http://top200mmo.com';
    }
}