<?php

declare(strict_types=1);

namespace App\Component\TopProcessor\Voter\Criteria\RageTopLike;


class GTop300UriCriteria extends AbstractUriCriteria
{
    public static function domain(): string
    {
        return 'http://gtop300.com';
    }
}