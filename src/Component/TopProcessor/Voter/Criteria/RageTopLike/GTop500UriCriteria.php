<?php

declare(strict_types=1);

namespace App\Component\TopProcessor\Voter\Criteria\RageTopLike;


class GTop500UriCriteria extends AbstractUriCriteria
{
    public static function domain(): string
    {
        return 'http://gtop500.com';
    }
}