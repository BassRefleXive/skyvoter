<?php

declare(strict_types=1);

namespace App\Component\TopProcessor\Voter\Criteria;


class MpoGTopUriCriteria extends VoteUriCriteria
{
    public static function domain(): string
    {
        return 'http://mpogtop.com';
    }

    public static function landing(string $id): self
    {
        return new self(
            'in/{id}',
            [
                'id' => $id,
            ]
        );
    }

    public static function vote(string $id): self
    {
        return new self(
            'in/{id}',
            [
                'id' => $id,
            ]
        );
    }
}