<?php

declare(strict_types = 1);

namespace App\Component\TopProcessor\Voter\Criteria;


use App\Component\TopProcessor\Exception\UriException;

class MmTop200UriCriteria extends VoteUriCriteria
{
    public static function domain(): string
    {
        return 'https://mmtop200.com';
    }

    public static function landing(string $id): self
    {
        return new self(
            'vote/{id}',
            [
                'id' => $id,
            ]
        );
    }

    public static function vote(string $id): self
    {
        throw UriException::notImplemented();
    }
}