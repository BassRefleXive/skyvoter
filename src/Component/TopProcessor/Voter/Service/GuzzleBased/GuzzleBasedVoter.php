<?php

declare(strict_types = 1);


namespace App\Component\TopProcessor\Voter\Service\GuzzleBased;


use App\Component\Captcha\ResolverRegistry;
use App\Component\Proxy\Service\ProxyIssuer;
use App\Component\TopProcessor\Voter\Service\AbstractVoter;
use App\Component\TopProcessor\Voter\Service\VoterInterface;
use GuzzleHttp\ClientInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;

abstract class GuzzleBasedVoter extends AbstractVoter implements VoterInterface
{
    private const HEADERS = [
        'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        'Accept-Language' => 'en-US,en;q=0.5',
        'Accept-Encoding' => 'gzip, deflate',
        'Cache-Control' => 'no-cache',
        'Connection' => 'keep-alive',
        'Pragma' => 'no-cache',
        'Upgrade-Insecure-Requests' => '1',
    ];

    private $client;
    private $userAgent;
    private $nodeManager;

    public function __construct(
        ResolverRegistry $resolverRegistry,
        ProxyIssuer $proxyIssuer,
        LoggerInterface $logger,
        ClientInterface $client,
        string $userAgent
    )
    {
        parent::__construct($resolverRegistry, $proxyIssuer, $logger);

        $this->client = $client;
        $this->userAgent = $userAgent;
        $this->nodeManager = new NodeManager();
    }

    protected final function client(): ClientInterface
    {
        return $this->client;
    }

    protected function landingResponse(string $endpoint): ResponseInterface
    {
        return $this->client()->request(
            'GET',
            $endpoint,
            [
                'headers' => $this->baseHeaders(),
            ]
        );
    }

    protected final function baseHeaders(): array
    {
        return array_merge(
            $this->topHeaders(),
            self::HEADERS,
            [
                'User-Agent' => $this->userAgent,
            ]
        );
    }

    protected final function mergedHeaders(array $headers): array
    {
        return array_merge(
            $this->topHeaders(),
            $headers,
            self::HEADERS
        );
    }

    protected final function nodeManager(): NodeManager
    {
        return $this->nodeManager;
    }

    abstract protected function topHeaders(): array;
}