<?php

declare(strict_types = 1);

namespace App\Component\TopProcessor\Voter\Service\GuzzleBased;


use App\Component\TopProcessor\Exception\VoterException;
use App\Component\TopProcessor\Voter\Command\VoteCommand;
use Symfony\Component\DomCrawler\Crawler;

class NodeManager
{
    public final function extractNodeAttributeValue(VoteCommand $command, Crawler $crawler, string $path, string $attribute): string
    {
        $node = $this->extractNode($command, $crawler, $path);

        $key = $node->attr($attribute);

        if (null === $key) {
            throw VoterException::nodeAttributeNotFound($command->id(), $command->top()->getValue(), $attribute);
        }

        return $key;
    }

    public final function extractNode(VoteCommand $command, Crawler $crawler, string $path): Crawler
    {
        $node = $crawler->filterXPath($path);

        if (!$node->count()) {
            throw VoterException::nodeNotFound($command->id(), $command->top()->getValue(), $path);
        }

        return $node;
    }
}