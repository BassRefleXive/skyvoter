<?php

declare(strict_types = 1);

namespace App\Component\TopProcessor\Voter\Service\GuzzleBased\Voter;


use App\Component\Captcha\Command\ResolveReCaptchaCommand;
use App\Component\Captcha\Enum\CaptchaType;
use App\Component\Proxy\Enum\ProxyResolver;
use App\Component\TopProcessor\Factory\UriFactory;
use App\Component\TopProcessor\Voter\Command\VoteCommand;
use App\Component\TopProcessor\Voter\Criteria\XTremeTop100UriCriteria;
use App\Component\TopProcessor\Voter\Service\GuzzleBased\GuzzleBasedVoter;
use GuzzleHttp\Cookie\CookieJar;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\DomCrawler\Crawler;

class XTremeTop100Voter extends GuzzleBasedVoter
{
    protected function supportProxyResolver(ProxyResolver $resolver): bool
    {
        return true;
    }

    public function doVote(VoteCommand $command): bool
    {
        $landingEndpoint = (new UriFactory())->create(XTremeTop100UriCriteria::landing($command->id()));

        $landing = $this->landingResponse($landingEndpoint);

        $landingContents = $landing->getBody()->getContents();

        $crawler = new Crawler($landingContents);

        $siteKey = $this->nodeManager()->extractNodeAttributeValue($command, $crawler, '//div[@data-sitekey]', 'data-sitekey');
        $secureCaptchaCheck = $this->nodeManager()->extractNodeAttributeValue($command, $crawler, '//input[@name="secure_captcha_check"]', 'value');
        $site = $this->nodeManager()->extractNodeAttributeValue($command, $crawler, '//input[@name="site"]', 'value');
        $ticki = $this->nodeManager()->extractNodeAttributeValue($command, $crawler, '//input[@name="ticki"]', 'value');

        $this->logger()->info(
            'XTremeTop100:Landing',
            [
                'endpoint'             => $landingEndpoint,
                'site_key'             => $siteKey,
                'secure_captcha_check' => $secureCaptchaCheck,
                'site'                 => $site,
                'ticki'                => $ticki,
            ]
        );

        $captcha = $this->resolveCaptcha(
            CaptchaType::byValue(CaptchaType::RE_CAPTCHA_2),
            $command->resolverCredentials(),
            new ResolveReCaptchaCommand($siteKey, $landingEndpoint)
        );

        $this->executeVote($command->id(), $landingEndpoint, $site, $secureCaptchaCheck, $ticki, $captcha);

        return true;
    }

    private function executeVote(string $serverId, string $landingEndpoint, string $site, string $secureCaptchaCheck, string $ticki, string $captcha): ResponseInterface
    {
        $cookieJar = CookieJar::fromArray([
            'flowridaoookie' => '1',
        ], 'www.xtremetop100.com');

        $result = $this->client()->request(
            'POST',
            (new UriFactory())->create(XTremeTop100UriCriteria::vote($serverId)),
            [
                'body'    => http_build_query([
                    'site'                 => $site,
                    'secure_captcha_check' => $secureCaptchaCheck,
                    'g-recaptcha-response' => $captcha,
                    'ticki'                => $ticki,
                ]),
                'cookies' => $cookieJar,
                'headers' => $this->mergedHeaders([
                    'Content-Type' => 'application/x-www-form-urlencoded',
                    'Referer'      => $landingEndpoint,
                ]),
            ]
        );

        $this->logger()->info('XTremeTop100:Success');

        return $result;
    }

    protected function topHeaders(): array
    {
        return [
            'Host' => 'www.xtremetop100.com',
        ];
    }
}