<?php

declare(strict_types=1);

namespace App\Component\TopProcessor\Voter\Service\GuzzleBased\Voter\RageTopLike;


use App\Component\Proxy\Enum\ProxyResolver;
use App\Component\TopProcessor\Factory\UriFactory;
use App\Component\TopProcessor\Voter\Command\VoteCommand;
use App\Component\TopProcessor\Voter\Service\GuzzleBased\GuzzleBasedVoter;
use GuzzleHttp\Exception\GuzzleException;

abstract class AbstractVoter extends GuzzleBasedVoter
{
    protected function supportProxyResolver(ProxyResolver $resolver): bool
    {
        return true;
    }

    protected function doVote(VoteCommand $command): bool
    {
        try {
            $this->client()->request(
                'POST',
                (new UriFactory())->create($this->uriBuilderClass()::vote($command->id())),
                [
                    'body' => http_build_query([
                        'vote' => 1,
                        'yes' => 'Yes',
                    ]),
                    'headers' => $this->mergedHeaders([
                        'Content-Type' => 'application/x-www-form-urlencoded',
                        'Referer' => (new UriFactory())->create($this->uriBuilderClass()::landing($command->id())),
                    ]),
                ]
            );
        } catch (GuzzleException $e) {
        }

        $this->logger()->info(sprintf('%s:Success', $this->name()));

        return true;
    }

    protected function topHeaders(): array
    {
        return [
            'Host' => $this->host(),
        ];
    }

    abstract protected function host(): string;

    abstract protected function uriBuilderClass(): string;

    abstract protected function name(): string;
}