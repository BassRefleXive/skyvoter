<?php

declare(strict_types=1);

namespace App\Component\TopProcessor\Voter\Service\GuzzleBased\Voter\RageTopLike;


use App\Component\TopProcessor\Voter\Criteria\RageTopLike\MmoRpg100UriCriteria;

class MmoRpg100Voter extends AbstractVoter
{
    protected function host(): string
    {
        return 'mmorpg-100.com';
    }

    protected function uriBuilderClass(): string
    {
        return MmoRpg100UriCriteria::class;
    }

    protected function name(): string
    {
        return 'MmoRpg100';
    }
}