<?php

declare(strict_types=1);

namespace App\Component\TopProcessor\Voter\Service\GuzzleBased\Voter\RageTopLike;


use App\Component\TopProcessor\Voter\Criteria\RageTopLike\Top100RageUriCriteria;

class Top100RageVoter extends AbstractVoter
{
    protected function host(): string
    {
        return 'top100rage.com';
    }

    protected function uriBuilderClass(): string
    {
        return Top100RageUriCriteria::class;
    }

    protected function name(): string
    {
        return 'Top100Rage';
    }
}