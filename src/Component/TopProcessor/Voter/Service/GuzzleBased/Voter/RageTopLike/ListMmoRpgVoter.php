<?php

declare(strict_types=1);

namespace App\Component\TopProcessor\Voter\Service\GuzzleBased\Voter\RageTopLike;


use App\Component\TopProcessor\Voter\Criteria\RageTopLike\ListMmoRpgUriCriteria;

class ListMmoRpgVoter extends AbstractVoter
{
    protected function host(): string
    {
        return 'listmmorpg.com';
    }

    protected function uriBuilderClass(): string
    {
        return ListMmoRpgUriCriteria::class;
    }

    protected function name(): string
    {
        return 'ListMmoRpg';
    }
}