<?php

declare(strict_types=1);

namespace App\Component\TopProcessor\Voter\Service\GuzzleBased\Voter\RageTopLike;


use App\Component\TopProcessor\Voter\Criteria\RageTopLike\Top200MmoUriCriteria;

class Top200MmoVoter extends AbstractVoter
{
    protected function host(): string
    {
        return 'top200mmo.com';
    }

    protected function uriBuilderClass(): string
    {
        return Top200MmoUriCriteria::class;
    }

    protected function name(): string
    {
        return 'Top200Mmo';
    }
}