<?php

declare(strict_types=1);

namespace App\Component\TopProcessor\Voter\Service\GuzzleBased\Voter\RageTopLike;


use App\Component\TopProcessor\Voter\Criteria\RageTopLike\GTop500UriCriteria;

class GTop500Voter extends AbstractVoter
{
    protected function host(): string
    {
        return 'gtop500.com';
    }

    protected function uriBuilderClass(): string
    {
        return GTop500UriCriteria::class;
    }

    protected function name(): string
    {
        return 'GTop500';
    }
}