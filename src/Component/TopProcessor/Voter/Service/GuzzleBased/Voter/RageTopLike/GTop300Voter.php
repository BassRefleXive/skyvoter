<?php

declare(strict_types=1);

namespace App\Component\TopProcessor\Voter\Service\GuzzleBased\Voter\RageTopLike;


use App\Component\TopProcessor\Voter\Criteria\RageTopLike\GTop300UriCriteria;

class GTop300Voter extends AbstractVoter
{
    protected function host(): string
    {
        return 'gtop300.com';
    }

    protected function uriBuilderClass(): string
    {
        return GTop300UriCriteria::class;
    }

    protected function name(): string
    {
        return 'GTop300';
    }
}