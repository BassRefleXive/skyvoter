<?php

declare(strict_types=1);

namespace App\Component\TopProcessor\Voter\Service\GuzzleBased\Voter\RageTopLike;


use App\Component\TopProcessor\Voter\Criteria\RageTopLike\TopMmoUriCriteria;

class TopMmoVoter extends AbstractVoter
{
    protected function host(): string
    {
        return 'top-mmo.com';
    }

    protected function uriBuilderClass(): string
    {
        return TopMmoUriCriteria::class;
    }

    protected function name(): string
    {
        return 'TopMmo';
    }
}