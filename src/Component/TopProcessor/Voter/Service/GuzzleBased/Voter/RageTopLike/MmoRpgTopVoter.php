<?php

declare(strict_types=1);

namespace App\Component\TopProcessor\Voter\Service\GuzzleBased\Voter\RageTopLike;


use App\Component\TopProcessor\Voter\Criteria\RageTopLike\MmoRpgTopUriCriteria;

class MmoRpgTopVoter extends AbstractVoter
{
    protected function host(): string
    {
        return 'mmorpg-top.com';
    }

    protected function uriBuilderClass(): string
    {
        return MmoRpgTopUriCriteria::class;
    }

    protected function name(): string
    {
        return 'MmoRpgTop';
    }
}