<?php

declare(strict_types=1);

namespace App\Component\TopProcessor\Voter\Service\GuzzleBased\Voter\RageTopLike;


use App\Component\TopProcessor\Voter\Criteria\RageTopLike\RageTopUriCriteria;

class RageTopVoter extends AbstractVoter
{
    protected function host(): string
    {
        return 'ragetop.com';
    }

    protected function uriBuilderClass(): string
    {
        return RageTopUriCriteria::class;
    }

    protected function name(): string
    {
        return 'RageTop';
    }
}