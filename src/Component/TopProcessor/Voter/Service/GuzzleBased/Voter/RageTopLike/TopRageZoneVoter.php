<?php

declare(strict_types=1);

namespace App\Component\TopProcessor\Voter\Service\GuzzleBased\Voter\RageTopLike;


use App\Component\TopProcessor\Voter\Criteria\RageTopLike\TopRageZoneUriCriteria;

class TopRageZoneVoter extends AbstractVoter
{
    protected function host(): string
    {
        return 'topragezone.com';
    }

    protected function uriBuilderClass(): string
    {
        return TopRageZoneUriCriteria::class;
    }

    protected function name(): string
    {
        return 'TopRageZone';
    }
}