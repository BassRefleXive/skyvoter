<?php

declare(strict_types=1);

namespace App\Component\TopProcessor\Voter\Service\GuzzleBased\Voter\RageTopLike;


use App\Component\TopProcessor\Voter\Criteria\RageTopLike\TopMmoRpgUriCriteria;

class TopMmoRpgVoter extends AbstractVoter
{
    protected function host(): string
    {
        return 'top-mmorpg.com';
    }

    protected function uriBuilderClass(): string
    {
        return TopMmoRpgUriCriteria::class;
    }

    protected function name(): string
    {
        return 'TopMmoRpg';
    }
}