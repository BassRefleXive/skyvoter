<?php

declare(strict_types=1);

namespace App\Component\TopProcessor\Voter\Service\GuzzleBased\Voter\RageTopLike;


use App\Component\TopProcessor\Voter\Criteria\RageTopLike\Top100MmoUriCriteria;

class Top100MmoVoter extends AbstractVoter
{
    protected function host(): string
    {
        return 'top100mmo.com';
    }

    protected function uriBuilderClass(): string
    {
        return Top100MmoUriCriteria::class;
    }

    protected function name(): string
    {
        return 'Top100Mmo';
    }
}