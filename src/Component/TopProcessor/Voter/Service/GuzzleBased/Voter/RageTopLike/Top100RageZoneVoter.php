<?php

declare(strict_types=1);

namespace App\Component\TopProcessor\Voter\Service\GuzzleBased\Voter\RageTopLike;


use App\Component\TopProcessor\Voter\Criteria\RageTopLike\Top100RageZoneUriCriteria;

class Top100RageZoneVoter extends AbstractVoter
{
    protected function host(): string
    {
        return 'top100ragezone.com';
    }

    protected function uriBuilderClass(): string
    {
        return Top100RageZoneUriCriteria::class;
    }

    protected function name(): string
    {
        return 'Top100RageZone';
    }
}