<?php

declare(strict_types = 1);

namespace App\Component\TopProcessor\Voter\Service\GuzzleBased\Voter;


use App\Component\Proxy\Enum\ProxyResolver;
use App\Component\TopProcessor\Factory\UriFactory;
use App\Component\TopProcessor\Voter\Command\VoteCommand;
use App\Component\TopProcessor\Voter\Criteria\RankingMuUriCriteria;
use App\Component\TopProcessor\Voter\Service\GuzzleBased\GuzzleBasedVoter;

class RankingMuVoter extends GuzzleBasedVoter
{
    protected function supportProxyResolver(ProxyResolver $resolver): bool
    {
        return true;
    }

    protected function doVote(VoteCommand $command): bool
    {
        $this->client()->request(
            'POST',
            (new UriFactory())->create(RankingMuUriCriteria::vote($command->id())),
            [
                'body'    => http_build_query([
                    'wid' => $command->id(),
                ]),
                'headers' => $this->mergedHeaders([
                    'Content-Type' => 'application/x-www-form-urlencoded',
                    'Referer'      => (new UriFactory())->create(RankingMuUriCriteria::landing($command->id())),
                ]),
            ]
        );

        $this->logger()->info('RankingMu:Success');

        return true;
    }

    protected function topHeaders(): array
    {
        return [
            'Host' => 'rankingmu.com',
        ];
    }
}