<?php

declare(strict_types=1);

namespace App\Component\TopProcessor\Voter\Service;

use App\Component\TopProcessor\Exception\TopProcessorRegistryException;
use App\Component\TopProcessor\Enum\Top;

class VoterRegistry
{
    private $voters = [];

    public function add(string $key, VoterInterface $processor): self
    {
        if (array_key_exists($key, $this->voters)) {
            throw TopProcessorRegistryException::alreadyExists($key);
        }

        $this->voters[$key] = $processor;

        return $this;
    }

    public function get(Top $top): VoterInterface
    {
        if (!array_key_exists($top->getValue(), $this->voters)) {
            throw TopProcessorRegistryException::notFound($top->getValue());
        }

        return $this->voters[$top->getValue()];
    }
}