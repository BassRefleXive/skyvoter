<?php

declare(strict_types = 1);


namespace App\Component\TopProcessor\Voter\Service;


use App\Component\TopProcessor\Voter\Command\VoteCommand;

interface VoterInterface
{
    public function vote(VoteCommand $command): bool;
}