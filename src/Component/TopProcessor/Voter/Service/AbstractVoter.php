<?php

declare(strict_types = 1);

namespace App\Component\TopProcessor\Voter\Service;


use App\Component\Captcha\Command\ResolveCommandInterface;
use App\Component\Captcha\Enum\CaptchaType;
use App\Component\Captcha\Model\ResolverCredentials;
use App\Component\Captcha\ResolverRegistry;
use App\Component\Proxy\Enum\ProxyResolver;
use App\Component\Proxy\Service\ProxyIssuer;
use App\Component\TopProcessor\Exception\UnsupportedProxyResolverException;
use App\Component\TopProcessor\Voter\Command\VoteCommand;
use Psr\Log\LoggerInterface;

abstract class AbstractVoter implements VoterInterface
{
    private $resolverRegistry;
    private $proxyIssuer;
    private $logger;

    public function __construct(
        ResolverRegistry $resolverRegistry,
        ProxyIssuer $proxyIssuer,
        LoggerInterface $logger
    )
    {
        $this->resolverRegistry = $resolverRegistry;
        $this->proxyIssuer = $proxyIssuer;
        $this->logger = $logger;
    }

    protected final function resolveCaptcha(
        CaptchaType $captchaType,
        ResolverCredentials $resolverCredentials,
        ResolveCommandInterface $command
    ): string
    {
        $captcha = null;

        $resolver = $this->resolverRegistry
            ->get($resolverCredentials->resolver());

        if ($captchaType->isReCaptcha2()) {
            $captcha = $resolver->resolveReCaptcha($resolverCredentials, $command);
        }

        if ($captchaType->isImageTotext()) {
            $captcha = $resolver->resolveImage($resolverCredentials, $command);
        }

        if ($captchaType->isFunCaptcha()) {
            $captcha = $resolver->resolveFunCaptcha($resolverCredentials, $command);
        }

        return $captcha;
    }

    protected final function logger(): LoggerInterface
    {
        return $this->logger;
    }

    protected final function logSubmissionStarted(VoteCommand $command): void
    {
        $this->logger()->info(
            'Started vote submission on server "{server}" in top "{top}"',
            [
                'server' => $command->id(),
                'top'    => $command->top()->getValue(),
            ]
        );
    }

    protected final function logSiteKeyExtracted(string $siteKey): void
    {
        $this->logger()->info('Site key "{site_key}" successfully extracted.', ['site_key' => $siteKey]);
    }

    protected final function logVoteSubmitted(): void
    {
        $this->logger->info('Captcha solved and vote form submitted.');
    }

    public function vote(VoteCommand $command): bool
    {
        if (!$this->supportProxyResolver($this->proxyIssuer->current()->resolvedBy())) {
            throw UnsupportedProxyResolverException::create($this->proxyIssuer->current()->resolvedBy());
        }

        return $this->doVote($command);
    }

    abstract protected function doVote(VoteCommand $voteCommand): bool;

    abstract protected function supportProxyResolver(ProxyResolver $resolver): bool;
}