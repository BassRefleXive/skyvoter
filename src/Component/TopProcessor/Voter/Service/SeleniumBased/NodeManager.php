<?php

declare(strict_types=1);

namespace App\Component\TopProcessor\Voter\Service\SeleniumBased;


use App\Component\TopProcessor\Exception\VoterException;
use App\Component\TopProcessor\Voter\Command\VoteCommand;
use Behat\Mink\Element\DocumentElement;
use Behat\Mink\Element\NodeElement;

class NodeManager
{
    public final function extractNodeAttributeValue(VoteCommand $command, DocumentElement $page, string $path, string $attribute): string
    {
        $node = $this->extractNode($command, $page, $path);

        if (!$node->hasAttribute($attribute)) {
            throw VoterException::nodeAttributeNotFound($command->id(), $command->top()->getValue(), $attribute);
        }

        return $node->getAttribute($attribute);
    }

    public final function extractNode(VoteCommand $command, DocumentElement $page, string $path): NodeElement
    {
        $node = $page->find('xpath', $path);

        if (null === $node) {
            throw VoterException::nodeNotFound($command->id(), $command->top()->getValue(), $path);
        }

        return $node;
    }
}