<?php

declare(strict_types=1);

namespace App\Component\TopProcessor\Voter\Service\SeleniumBased\Voter;


use App\Component\Captcha\Command\ResolveReCaptchaCommand;
use App\Component\Captcha\Enum\CaptchaType;
use App\Component\Proxy\Enum\ProxyResolver;
use App\Component\TopProcessor\Factory\UriFactory;
use App\Component\TopProcessor\Voter\Command\VoteCommand;
use App\Component\TopProcessor\Voter\Criteria\MmoTop100UriCriteria;
use App\Component\TopProcessor\Voter\Service\SeleniumBased\SeleniumBasedVoter;
use Behat\Mink\Session;

class MmoTop100Voter extends SeleniumBasedVoter
{
    protected function landing(VoteCommand $command): string
    {
        return (new UriFactory())->create(MmoTop100UriCriteria::landing($command->id()));
    }

    protected function supportProxyResolver(ProxyResolver $resolver): bool
    {
        return true;
    }

    public function doVote(VoteCommand $command): bool
    {
        $this->sessionIssuer()->current()->restart();

        return parent::vote($command);
    }

    protected function submit(VoteCommand $command, Session $session): bool
    {
        $this->logSubmissionStarted($command);

        $page = $session->getPage();

        $siteKey = $this->nodeManager()->extractNodeAttributeValue($command, $page, '//div[@data-sitekey]', 'data-sitekey');

        $this->logSiteKeyExtracted($siteKey);

        $captcha = $this->resolveCaptcha(
            CaptchaType::byValue(CaptchaType::RE_CAPTCHA_2),
            $command->resolverCredentials(),
            new ResolveReCaptchaCommand($siteKey, $session->getCurrentUrl())
        );

        $session->executeScript(
            sprintf("document.getElementById('g-recaptcha-response').innerHTML = '%s';", $captcha)
        );

        $this->nodeManager()->extractNode($command, $page, '//button[@type="submit"]')->click();

        $this->logVoteSubmitted();

        return true;
    }
}