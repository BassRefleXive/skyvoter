<?php

declare(strict_types=1);

namespace App\Component\TopProcessor\Voter\Service\SeleniumBased\Voter;


use App\Component\Proxy\Enum\ProxyResolver;
use App\Component\TopProcessor\Factory\UriFactory;
use App\Component\TopProcessor\Voter\Command\VoteCommand;
use App\Component\TopProcessor\Voter\Criteria\MpoGTopUriCriteria;
use App\Component\TopProcessor\Voter\Service\SeleniumBased\SeleniumBasedVoter;
use Behat\Mink\Session;

class MpoGTopVoter extends SeleniumBasedVoter
{
    protected function landing(VoteCommand $command): string
    {
        return (new UriFactory())->create(MpoGTopUriCriteria::landing($command->id()));
    }

    protected function supportProxyResolver(ProxyResolver $resolver): bool
    {
        return true;
    }

    protected function doVote(VoteCommand $voteCommand): bool
    {
        return parent::vote($voteCommand);
    }

    protected function submit(VoteCommand $command, Session $session): bool
    {
        $this->logSubmissionStarted($command);

        $page = $session->getPage();

        $this->nodeManager()->extractNode($command, $page, '//input[@type="submit"]')->submit();

        $this->logVoteSubmitted();

        return true;
    }
}