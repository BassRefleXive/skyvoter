<?php

declare(strict_types = 1);


namespace App\Component\TopProcessor\Voter\Service\SeleniumBased;


use App\Component\Captcha\ResolverRegistry;
use App\Component\Proxy\Service\ProxyIssuer;
use App\Component\TopProcessor\SecurityPass\SecurityPassChain;
use App\Component\TopProcessor\Session\SessionIssuer;
use App\Component\TopProcessor\Voter\Command\VoteCommand;
use App\Component\TopProcessor\Voter\Service\AbstractVoter;
use Behat\Mink\Session;
use Psr\Log\LoggerInterface;

abstract class SeleniumBasedVoter extends AbstractVoter
{
    private $securityPassChain;
    private $sessionIssuer;
    private $nodeManager;

    public function __construct(
        SecurityPassChain $securityPassChain,
        ResolverRegistry $resolverRegistry,
        ProxyIssuer $proxyIssuer,
        SessionIssuer $sessionIssuer,
        LoggerInterface $logger
    )
    {
        parent::__construct($resolverRegistry, $proxyIssuer, $logger);

        $this->securityPassChain = $securityPassChain;
        $this->sessionIssuer = $sessionIssuer;

        $this->nodeManager = new NodeManager();
    }

    public function vote(VoteCommand $command): bool
    {
        $session = $this->sessionIssuer->current();
        $session->visit($this->landing($command));

        $session = $this->securityPassChain->clear($command->resolverCredentials(), $session);

        return $this->submit($command, $session);
    }

    protected final function sessionIssuer(): SessionIssuer
    {
        return $this->sessionIssuer;
    }

    protected final function nodeManager(): NodeManager
    {
        return $this->nodeManager;
    }

    abstract protected function landing(VoteCommand $command): string;

    abstract protected function submit(VoteCommand $command, Session $session): bool;
}