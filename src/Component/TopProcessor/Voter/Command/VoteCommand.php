<?php

declare(strict_types=1);

namespace App\Component\TopProcessor\Voter\Command;


use App\Component\Captcha\Model\ResolverCredentials;
use App\Component\TopProcessor\Enum\Top;

class VoteCommand
{
    private $top;
    private $resolverCredentials;
    private $id;

    public function __construct(Top $top, ResolverCredentials $resolverCredentials, string $id)
    {
        $this->top = $top;
        $this->resolverCredentials = $resolverCredentials;
        $this->id = $id;
    }

    public function top(): Top
    {
        return $this->top;
    }

    public function resolverCredentials(): ResolverCredentials
    {
        return $this->resolverCredentials;
    }

    public function id(): string
    {
        return $this->id;
    }
}