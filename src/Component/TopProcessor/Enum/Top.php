<?php

declare(strict_types = 1);

namespace App\Component\TopProcessor\Enum;


use MabeEnum\Enum;

final class Top extends Enum
{
    public const TOP_SERVERS_200 = 'topservers200';
    public const GAMES_TOP_100 = 'gamestop100';
    public const MPO_GTOP = 'mpogtop';
    public const TOP_100_ARENA = 'top100arena';
    public const XTREME_TOP_100 = 'xtremetop100';
    public const E_TOP_GAMES = 'etopgames';
    public const MMO_SERVER_PRO = 'mmoserverpro';
    public const ARENA_TOP_100 = 'arenatop100';
    public const TOP_MMO_RPG_SERVERS = 'topmmorpgservers';
    public const MM_TOP_200 = 'mmtop200';
    public const RANKING_MU = 'rankingmu';
    public const MMO_TOP_100 = 'mmotop100';
    public const TOP_100_RAGE_ZONE = 'top100ragezone';
    public const RAGE_TOP = 'ragetop';
    public const LIST_MMO_RPG = 'listmmorpg';
    public const TOP_MMO = 'topmmo';
    public const TOP_100_MMO = 'top100mmo';
    public const TOP_200_MMO = 'top200mmo';
    public const MMO_RPG_100 = 'mmorpg100';
    public const TOP_RAGE_ZONE = 'topragezone';
    public const TOP_100_RAGE = 'top100rage';
    public const G_TOP_300 = 'gtop300';
    public const G_TOP_500 = 'gtop500';
    public const MMO_RPG_TOP = 'mmorpgtop';
    public const TOP_MMO_RPG = 'topmmorpg';

    public function isMpoGTop(): bool
    {
        return $this->is(self::MPO_GTOP);
    }
}