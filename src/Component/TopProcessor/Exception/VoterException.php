<?php

declare(strict_types=1);

namespace App\Component\TopProcessor\Exception;


class VoterException extends \RuntimeException implements TopProcessorExceptionInterface
{
    public static final function nodeNotFound(string $serverId, string $top, string $node): self
    {
        return new self(sprintf('Failed to extract node for server "%s" in top "%s". Missing node "%s".', $serverId, $top, $node));
    }

    public static final function nodeAttributeNotFound(string $serverId, string $top, string $node): self
    {
        return new self(sprintf('Failed to extract node attribute for server "%s" in top "%s". Missing node "%s".', $serverId, $top, $node));
    }
}