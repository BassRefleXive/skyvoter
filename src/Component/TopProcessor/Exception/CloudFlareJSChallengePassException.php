<?php

declare(strict_types=1);

namespace App\Component\TopProcessor\Exception;


class CloudFlareJSChallengePassException extends SecurityPassException
{
    public static final function maxRetriesExceeded(): self
    {
        return new self('Exceeded count of max retries when trying to bypass CF JS challenge protected page.');
    }
}