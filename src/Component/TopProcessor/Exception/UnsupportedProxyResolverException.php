<?php

declare(strict_types = 1);


namespace App\Component\TopProcessor\Exception;


use App\Component\Proxy\Enum\ProxyResolver;

class UnsupportedProxyResolverException extends \RuntimeException implements TopProcessorExceptionInterface
{
    public static final function create(ProxyResolver $resolver): self
    {
        return new self(
            sprintf(
                'Proxy resolver "%s" not supported.',
                $resolver->getName()
            )
        );
    }
}