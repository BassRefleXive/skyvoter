<?php

declare(strict_types = 1);


namespace App\Component\TopProcessor\Exception;


class TopProcessorRegistryException extends \LogicException
{
    public static function alreadyExists(string $key): self
    {
        return new self(sprintf('Processor with key "%s" already exists in registry.', $key));
    }

    public static function notFound(string $key): self
    {
        return new self(sprintf('Processor with key "%s" not fount in registry.', $key));
    }
}