<?php

declare(strict_types=1);

namespace App\Component\TopProcessor\Exception;


class CloudFlareReCaptchaProtectedPagePassException extends SecurityPassException
{
    public static final function maxRetriesExceeded(): self
    {
        return new self('Exceeded count of max retries when trying to bypass CF reCaptcha protected page.');
    }
}