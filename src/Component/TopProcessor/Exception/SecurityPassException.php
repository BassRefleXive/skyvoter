<?php

declare(strict_types=1);

namespace App\Component\TopProcessor\Exception;


class SecurityPassException extends \RuntimeException implements TopProcessorExceptionInterface
{
    public static final function noPassDefined(): self
    {
        return new self('No security passes defined.');
    }
}