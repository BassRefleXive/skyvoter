<?php

declare(strict_types=1);

namespace App\Component\TopProcessor\Factory;


use App\Component\Core\Http\Criteria\UriCriteria;
use App\Component\TopProcessor\Voter\Criteria\VoteUriCriteria;
use App\Component\Core\Http\Factory\UriFactory as BaseUriFactory;

class UriFactory extends BaseUriFactory
{
    private $domain;

    /**
     * @param VoteUriCriteria $uriCriteria
     * @return string
     */
    public function create(UriCriteria $uriCriteria): string
    {
        $this->domain = $uriCriteria::domain();

        return parent::create($uriCriteria);
    }

    protected function domain(): string
    {
        return $this->domain;
    }
}