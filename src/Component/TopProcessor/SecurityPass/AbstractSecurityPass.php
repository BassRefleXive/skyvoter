<?php

declare(strict_types=1);

namespace App\Component\TopProcessor\SecurityPass;


use App\Component\Captcha\Model\ResolverCredentials;
use Behat\Mink\Session;

abstract class AbstractSecurityPass
{
    /**
     * @var AbstractSecurityPass
     */
    protected $next;

    public function setNext(self $next)
    {
        $this->next = $next;
    }

    public function clear(ResolverCredentials $resolverCredentials, Session $session): Session
    {
        $session = $this->doClear($resolverCredentials, $session);

        if (null === $this->next) {
            return $session;
        }

        return $this->next->clear($resolverCredentials, $session);
    }

    abstract protected function doClear(ResolverCredentials $resolverCredentials, Session $session): Session;
}