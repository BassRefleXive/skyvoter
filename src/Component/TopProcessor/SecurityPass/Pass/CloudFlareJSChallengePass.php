<?php

declare(strict_types = 1);


namespace App\Component\TopProcessor\SecurityPass\Pass;


use App\Component\Captcha\Model\ResolverCredentials;
use App\Component\TopProcessor\Exception\CloudFlareJSChallengePassException;
use App\Component\TopProcessor\SecurityPass\AbstractSecurityPass;
use Behat\Mink\Session;
use Psr\Log\LoggerInterface;

class CloudFlareJSChallengePass extends AbstractSecurityPass
{
    private $maxRetries;
    private $logger;

    public function __construct(int $maxRetries, LoggerInterface $logger)
    {
        $this->maxRetries = $maxRetries;
        $this->logger = $logger;
    }

    protected function doClear(ResolverCredentials $resolverCredentials, Session $session): Session
    {
        return $this->bypass($resolverCredentials, $session);
    }

    private function bypass(ResolverCredentials $resolverCredentials, Session $session, int $try = 1): Session
    {
        if ($try > $this->maxRetries) {
            throw CloudFlareJSChallengePassException::maxRetriesExceeded();
        }

        if (!$this->isProtected($session)) {
            $this->logger->info('Page is without CF JS challenge protection.');

            return $session;
        }

        $this->logger->warning(sprintf('Page CF JS challenge protected. Trying to bypass. Try: #%d.', $try));

        $session->wait(8000);

        return $session;
    }

    private function isProtected(Session $session): bool
    {
        $page = $session->getPage();

        $content = $page->getContent();

        return (
            false !== strpos($content, "jschl_vc") &&
            false !== strpos($content, "pass") &&
            false !== strpos($content, "jschl_answer") &&
            false !== strpos($content, "/cdn-cgi/l/chk_jschl")
        );
    }
}