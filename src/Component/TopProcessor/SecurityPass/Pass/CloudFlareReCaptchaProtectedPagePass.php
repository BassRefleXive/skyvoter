<?php

declare(strict_types=1);

namespace App\Component\TopProcessor\SecurityPass\Pass;


use App\Component\Captcha\Command\ResolveReCaptchaCommand;
use App\Component\Captcha\Model\ResolverCredentials;
use App\Component\Captcha\ResolverRegistry;
use App\Component\TopProcessor\Exception\CloudFlareReCaptchaProtectedPagePassException;
use App\Component\TopProcessor\SecurityPass\AbstractSecurityPass;
use Behat\Mink\Session;
use Psr\Log\LoggerInterface;

class CloudFlareReCaptchaProtectedPagePass extends AbstractSecurityPass
{
    private $resolverRegistry;
    private $maxRetries;
    private $logger;

    public function __construct(ResolverRegistry $resolverRegistry, int $maxRetries, LoggerInterface $logger)
    {
        $this->resolverRegistry = $resolverRegistry;
        $this->maxRetries = $maxRetries;
        $this->logger = $logger;
    }

    protected function doClear(ResolverCredentials $resolverCredentials, Session $session): Session
    {
        return $this->bypass($resolverCredentials, $session);
    }

    private function bypass(ResolverCredentials $resolverCredentials, Session $session, int $try = 1): Session
    {
        if ($try > $this->maxRetries) {
            throw CloudFlareReCaptchaProtectedPagePassException::maxRetriesExceeded();
        }

        if (!$this->isProtected($session)) {
            $this->logger->info('Page is without CF reCaptcha protection.');

            return $session;
        }

        $this->logger->warning(sprintf('Page CF reCaptcha protected. Trying to bypass. Try: #%d.', $try));

        $page = $session->getPage();

        $siteKey = $page->find('xpath', '//script[@data-sitekey]')->getAttribute('data-sitekey');

        $this->logger->info('Site key "{site_key}" successfully extracted.', ['site_key' => $siteKey]);

        $captcha = $this->resolverRegistry
            ->get($resolverCredentials->resolver())
            ->resolveReCaptcha(
                $resolverCredentials,
                new ResolveReCaptchaCommand($siteKey, $session->getCurrentUrl())
            );

        $session->executeScript(
            sprintf("document.getElementById('g-recaptcha-response').innerHTML = '%s';", $captcha)
        );

        $page->findById('challenge-form')->submit();

        $this->logger->info('Captcha solved and CF protected page form submitted. Waiting for result.');

        return $this->bypass($resolverCredentials, $session, ++$try);
    }

    private function isProtected(Session $session): bool
    {
        $page = $session->getPage();

        return null !== $page->findById('cf-wrapper') && null !== $page->find('xpath', '//script[@data-sitekey]');
    }
}