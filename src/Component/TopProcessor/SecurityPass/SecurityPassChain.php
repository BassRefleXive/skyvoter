<?php

declare(strict_types=1);

namespace App\Component\TopProcessor\SecurityPass;


use App\Component\Captcha\Model\ResolverCredentials;
use App\Component\TopProcessor\Exception\SecurityPassException;
use Behat\Mink\Session;

class SecurityPassChain
{
    /**
     * @var AbstractSecurityPass|null
     */
    protected $first = null;

    public function __construct(array $parsers)
    {
        $current = null;

        foreach ($parsers as $parser) {
            if (is_null($this->first)) {
                $this->first = $parser;
            }

            if (!is_null($current)) {
                $current->setNext($parser);
            }
            $current = $parser;
        }
    }

    public function clear(ResolverCredentials $resolverCredentials, Session $session): Session
    {
        if ($this->first) {
            return $this->first->clear($resolverCredentials, $session);
        }

        throw SecurityPassException::noPassDefined();
    }
}