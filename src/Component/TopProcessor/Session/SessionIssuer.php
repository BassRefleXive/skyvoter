<?php

declare(strict_types = 1);

namespace App\Component\TopProcessor\Session;


use App\Component\Proxy\Service\ProxyIssuer;
use Behat\Mink\Driver\Selenium2Driver;
use Behat\Mink\Mink;
use Behat\Mink\Session;

class SessionIssuer
{
    private $browserName;
    private $browserHost;
    private $pageLoadTimeout;
    private $proxyIssuer;

    /**
     * @var Mink
     */
    private $mink;

    public function __construct(ProxyIssuer $proxyIssuer, string $browserName, string $browserHost, int $pageLoadTimeout)
    {
        $this->proxyIssuer = $proxyIssuer;
        $this->browserName = $browserName;
        $this->browserHost = $browserHost;
        $this->pageLoadTimeout = $pageLoadTimeout;
    }

    public function current(): Session
    {
        if (null === $this->mink) {
            return $this->next();
        }

        return $this->mink->getSession('session');
    }

    public function next(): Session
    {
        if (null !== $this->mink) {
            $this->current()->stop();
        }

        $this->init();

        return $this->mink->getSession('session');
    }

    public function restart(): void
    {
        if (null === $this->mink) {
            return;
        }

        $this->current()->restart();
    }

    private function init(): void
    {
        $driver = new Selenium2Driver(
            $this->browserName,
            [
                'proxy' => [
                    'proxyType'  => 'manual',
                    'httpProxy'  => sprintf('%s:%s', $this->proxyIssuer->current()->ip(), $this->proxyIssuer->current()->port()),
                    'sslProxy'  => sprintf('%s:%s', $this->proxyIssuer->current()->ip(), $this->proxyIssuer->current()->port()),
                ],
            ],
            $this->browserHost
        );

        $driver->setTimeouts([
            'page load' => $this->pageLoadTimeout * 1000,
        ]);

        $this->mink = new Mink([
            'session' => new Session($driver),
        ]);
    }
}