<?php

declare(strict_types=1);

namespace App\Component\Core\Http\Exception;


class HttpClientException extends \RuntimeException
{
}