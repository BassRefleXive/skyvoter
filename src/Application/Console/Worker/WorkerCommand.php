<?php

namespace App\Application\Console\Worker;

use App\Component\Captcha\Exception\CaptchaResolverException;
use App\Component\Captcha\Model\ResolverCredentials;
use App\Component\DirectorConnector\JobService;
use App\Component\DirectorConnector\Model\Job;
use App\Component\IpLookup\IpDiscoverer;
use App\Component\Proxy\Service\ProxyIssuer;
use App\Component\TopProcessor\Exception\SecurityPassException;
use App\Component\TopProcessor\Exception\TopProcessorExceptionInterface;
use App\Component\TopProcessor\Exception\UnsupportedProxyResolverException;
use App\Component\TopProcessor\Session\SessionIssuer;
use App\Component\TopProcessor\Voter\Command\VoteCommand;
use App\Component\TopProcessor\Voter\Service\VoterRegistry;
use App\Component\Tor\ProxyChanger;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Stopwatch\Stopwatch;
use WebDriver\Exception;

class WorkerCommand extends Command
{
    private const NO_JOBS_WAIT_SECONDS = 5;
    private const VOTE_TRY_COUNT_ON_EXCEPTION = 3;

    private $voterRegistry;
    private $proxyChanger;
    private $ipDiscoverer;
    private $jobService;
    private $sessionIssuer;
    private $proxyIssuer;
    private $logger;

    public function __construct(
        VoterRegistry $voterRegistry,
        ProxyChanger $proxyChanger,
        IpDiscoverer $ipDiscoverer,
        JobService $jobService,
        SessionIssuer $sessionIssuer,
        ProxyIssuer $proxyIssuer,
        LoggerInterface $logger
    )
    {
        parent::__construct();

        $this->voterRegistry = $voterRegistry;
        $this->proxyChanger = $proxyChanger;
        $this->ipDiscoverer = $ipDiscoverer;
        $this->jobService = $jobService;
        $this->sessionIssuer = $sessionIssuer;
        $this->proxyIssuer = $proxyIssuer;
        $this->logger = $logger;
    }

    protected function configure()
    {
        $this->setName('app:worker');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $io->title('Votes Worker');

        $this->logger->info('Worker Started Up');

        while (true) {
            $stopwatch = new Stopwatch();
            $stopwatch->start($this->getName());

            $this->logger->info('Fetching jobs to execute.');
            $jobs = $this->jobService->next();

            $this->logger->info('Fetched {count} jobs to execute. Starting work.', ['count' => $jobs->count()]);

            if (0 === $jobs->count()) {
                $event = $stopwatch->stop($this->getName());

                $this->logger->info('There is no jobs to execute. Done in {seconds}, {memory}MB used. Waiting {wait_seconds} before next try.', [
                    'seconds' => $event->getDuration() / 1000,
                    'memory' => $event->getMemory() / 1024 / 1024,
                    'wait_seconds' => self::NO_JOBS_WAIT_SECONDS,
                ]);

                sleep(self::NO_JOBS_WAIT_SECONDS);

                continue;
            }

            try {

                $proxy = $this->proxyIssuer->next();
                $this->sessionIssuer->current()->restart();


                /** @var Job $job */
                foreach ($jobs->all() as $job) {
                    try {
                        $command = (new VoteCommand(
                            $job->top(),
                            new ResolverCredentials($job->captchaResolver()->resolver(), $job->captchaResolver()->key()),
                            $job->serverId()
                        ));

                        $try = 0;

                        do {
                            $try++;
                            $this->logger->info(sprintf('Performing requests with proxy: %s:%d.', $proxy->ip(), $proxy->port()));

                            try {
                                try {
                                    $result = $this->voterRegistry
                                        ->get($job->top())
                                        ->vote($command);
                                } catch (SecurityPassException $e) {
                                    $this->logger->error('Unable to pass security checks. Switching session and trying again.');

                                    $this->sessionIssuer->next();

                                    throw $e;
                                } catch (CaptchaResolverException $e) {
                                    $this->logger->error('Failed to resolve captcha. Trying to vote again.', ['error' => $e->getMessage()]);

                                    $this->proxyChanger->change();
                                    $this->sessionIssuer->next();

                                    throw $e;
                                } catch (Exception $e) {
                                    if (Exception::TIMEOUT == $e->getCode() && $job->top()->isMpoGTop()) {
                                        $result = true;

                                        break;
                                    }

                                    $this->logger->alert('Connection timed out. Restart session and continue voting.');

                                    $this->sessionIssuer->next();

                                    throw $e;
                                } catch (UnsupportedProxyResolverException $e) {
                                    $this->logger->alert('This server does not support this type of proxy.');
                                    $try = self::VOTE_TRY_COUNT_ON_EXCEPTION;

                                    throw $e;
                                } catch (TopProcessorExceptionInterface $e) {
                                    $this->logger->alert('Found TopProcessor issue: "{issue}".', [
                                        'issue' => $e->getMessage(),
                                    ]);

                                    $this->sessionIssuer->next();

                                    throw $e;
                                }
                            } catch (\Throwable $e) {
                                $result = false;

                                $this->logger->alert('Issue raised current retry: "{issue}". Trying to vote again {try} of {tries} times.', [
                                    'issue' => $e->getMessage(),
                                    'try' => $try,
                                    'tries' => self::VOTE_TRY_COUNT_ON_EXCEPTION,
                                ]);
                            }
                        } while (!$result && $try < self::VOTE_TRY_COUNT_ON_EXCEPTION);

                        if ($result) {
                            $this->jobService->execute($job);
                        } else {
                            $this->logger->error('Exceeded amount of retries to make a vote.');

                            $this->jobService->fail($job);
                        }

                        $this->logger->info('Job for top {top} finished. State: {state}.', [
                            'top' => $job->top()->getName(),
                            'state' => $result ? 'SUCCESS' : 'FAILURE',
                        ]);
                    } catch (\Throwable $e) {
                        $this->logger->error('Failing job. Fatal error: {error}.', [
                            'error' => $e->getMessage(),
                        ]);

                        $this->jobService->fail($job);
                    }
                }

            } catch (\Throwable $e) {
                foreach ($jobs as $job) {
                    $this->jobService->fail($job);
                }
            }

            $event = $stopwatch->stop($this->getName());

            $this->logger->info('Job done in {seconds}, {memory}MB used.', [
                'seconds' => $event->getDuration() / 1000,
                'memory' => $event->getMemory() / 1024 / 1024,
            ]);
        }
    }
}