<?php

declare(strict_types=1);

namespace App\Application\Console\WebDriven;


use App\Component\IpLookup\IpDiscoverer;
use App\Component\Proxy\Service\ProxyIssuer;
use Behat\Mink\Driver\Selenium2Driver;
use Behat\Mink\Mink;
use Behat\Mink\Session;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Stopwatch\Stopwatch;

class TestCommand extends Command
{
    private $proxyIssuer;
    private $client;
    private $ipDiscoverer;

    public function __construct(ProxyIssuer $proxyIssuer, ClientInterface $client, IpDiscoverer $ipDiscoverer)
    {
        parent::__construct();

        $this->proxyIssuer = $proxyIssuer;
        $this->client = $client;
        $this->ipDiscoverer = $ipDiscoverer;
    }

    protected function configure()
    {
        $this->setName('app:web-driven:test');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $cnt = 0;
        $proxies = 0;


//        do {
//            try {
//                $proxies++;
//
//                $proxy = $this->proxyIssuer->next();
//
//                $output->writeln([
//                    '',
//                    sprintf('Expected: %s', $proxy->ip()),
//                ]);
//                $stopwatch = new Stopwatch();
//                $stopwatch->start($this->getName());
//                $actualIp = $this->ipDiscoverer->discover();
//                $event = $stopwatch->stop($this->getName());
//                $output->writeln(sprintf('Actual: %s; Done in %.3f seconds.', $actualIp, $event->getDuration() / 1000));
//
//                if ($proxy->ip() !== $actualIp) {
//                    $output->writeln('Proxy IP not matched real IP. Performing some requests to check is it changing all the time.');
//
//                    for ($i = 0; $i < 5; $i++) {
//                        $stopwatch = new Stopwatch();
//                        $stopwatch->start($this->getName());
//                        $actualIp = $this->ipDiscoverer->discover();
//                        $event = $stopwatch->stop($this->getName());
//                        $output->writeln(sprintf('Actual: %s; Done in %.3f seconds.', $actualIp, $event->getDuration() / 1000));
//                    }
//                }
//
//            } catch (RequestException $e) {
//                $output->writeln($e->getMessage());
//
//                continue;
//            }
//
//            $cnt++;
//        } while ($cnt < 10);
//
//        echo sprintf('Tried %d proxies.', $proxies) . PHP_EOL;

        $proxyHost = '34.203.233.13';
        $proxyPort = '80';

        $driver = new Selenium2Driver(
            'chrome',
            [
                'proxy' => [
                    'proxyType' => 'manual',
                    'httpProxy' => sprintf('%s:%s', $proxyHost, $proxyPort),
                    'sslProxy' => sprintf('%s:%s', $proxyHost, $proxyPort),
                    'socksProxy' => sprintf('%s:%s', $proxyHost, $proxyPort),
                ]
            ],
            'http://chrome:4444/wd/hub'
        );

        $mink = new Mink([
            'session' => new Session($driver)
        ]);

        $mink->getSession('session')->visit('https://api.ipify.org?format=json');
        $mink->getSession('session')->wait(10000);
    }
}