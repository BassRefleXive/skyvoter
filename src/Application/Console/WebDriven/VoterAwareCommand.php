<?php

declare(strict_types=1);

namespace App\Application\Console\WebDriven;

use App\Component\Captcha\Enum\Resolver;
use App\Component\Captcha\Exception\CaptchaResolverException;
use App\Component\Captcha\Model\ResolverCredentials;
use App\Component\Proxy\Enum\ProxyResolver;
use App\Component\Proxy\Service\ProxyIssuer;
use App\Component\TopProcessor\Enum\Top;
use App\Component\TopProcessor\Exception\SecurityPassException;
use App\Component\TopProcessor\Session\SessionIssuer;
use App\Component\TopProcessor\Voter\Command\VoteCommand;
use App\Component\TopProcessor\Voter\Service\VoterRegistry;
use App\Component\Tor\ProxyChanger;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Stopwatch\Stopwatch;
use WebDriver\Exception;

class VoterAwareCommand extends Command
{
    private $proxyChanger;
    private $voterRegistry;
    private $sessionIssuer;
    private $proxyIssuer;
    private $logger;
    private $usedIps;

    public function __construct(
        ProxyChanger $proxyChanger,
        VoterRegistry $voterRegistry,
        SessionIssuer $sessionIssuer,
        ProxyIssuer $proxyIssuer,
        LoggerInterface $logger
    )
    {
        parent::__construct();

        $this->proxyChanger = $proxyChanger;
        $this->voterRegistry = $voterRegistry;
        $this->sessionIssuer = $sessionIssuer;
        $this->proxyIssuer = $proxyIssuer;
        $this->logger = $logger;
        $this->usedIps = [];
    }

    protected function configure()
    {
        $this->setName('app:web-driven:voter-aware:vote')
            ->addArgument('top', InputArgument::REQUIRED, 'Top')
            ->addArgument('server', InputArgument::REQUIRED, 'Server ID')
            ->addArgument('count', InputArgument::REQUIRED, 'Count of Votes')
            ->addArgument('resolver', InputArgument::REQUIRED, 'Captcha Resolver')
            ->addArgument('resolver_key', InputArgument::REQUIRED, 'Captcha Resolver Key')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $server = $input->getArgument('server');
        $top = $input->getArgument('top');

        $io->title(sprintf('Voting for server "%s" on top "%s".', $server, $top));

        $stopwatch = new Stopwatch();
        $stopwatch->start($this->getName());

        $proxy = $this->proxyIssuer->next();

        $submitted = 0;

        do {
            $this->logger->info(sprintf('Performing requests with proxy: %s:%d.', $proxy->ip(), $proxy->port()));

            try {
                $this->voterRegistry
                    ->get(Top::byValue($top))
                    ->vote($this->command($input));

                $proxy = $this->proxyIssuer->next();
                $this->sessionIssuer->next();

                $submitted++;
            } catch (SecurityPassException $e) {
                $this->logger->error('Unable to pass security checks with IP "{ip}". Switching session and ip and trying again.', ['ip' => $proxy->ip()]);

                $proxy = $this->proxyIssuer->next();
                $this->sessionIssuer->next();

                continue;
            } catch (Exception $e) {
                if (Exception::TIMEOUT === $e->getCode()) {
                    $proxy = $this->proxyIssuer->next();
                    $this->sessionIssuer->next();
                    $this->logger->info('Connection timed out. Restart session and continue voting.');

                    continue;
                }

                throw $e;
            } catch (CaptchaResolverException $e) {
                $this->logger->error('Failed to resolve captcha. Trying to vote again.', ['error' => $e->getMessage()]);

                $proxy = $this->proxyIssuer->next();
                $this->sessionIssuer->next();

                continue;
            }
        } while ($submitted < $this->count($input));

        $event = $stopwatch->stop($this->getName());

        $io->newLine();
        $io->success(
            sprintf(
                'Done in %.3f seconds, %.3f MB memory used.',
                $event->getDuration() / 1000,
                $event->getMemory() / 1024 / 1024
            )
        );
    }

    protected final function command(InputInterface $input): VoteCommand
    {
        return new VoteCommand(
            Top::byValue($input->getArgument('top')),
            new ResolverCredentials(Resolver::byValue($input->getArgument('resolver')), $input->getArgument('resolver_key')),
            $input->getArgument('server')
        );
    }

    private function count(InputInterface $input): int
    {
        return (int) $input->getArgument('count');
    }
}