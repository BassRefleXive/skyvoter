#!/bin/bash

export TAG=%env.TAG%
docker login --username=%system.docker_registry_user% --password=%system.docker_registry_password% %system.docker_registry_host%

docker pull docker-registry.myenv.tk/vote-worker-tor:%env.TAG%
docker pull docker-registry.myenv.tk/vote-worker-app:%env.TAG%
docker pull docker-registry.myenv.tk/vote-worker-privoxy:%env.TAG%

CONTAINER_IDS="$(docker ps -aqf "label=voter-worker" -aqf "status=running" | paste -sd " " -)"
IFS=' ' read -r -a array <<< $CONTAINER_IDS

for id in "${array[@]}"
do
	docker kill $id
	docker rm $id

	echo "Killed and removed container with ID: ${id};"
done

for (( i=0; i < %Replicas%; i++ ))
do
    TOR_PUBLIC_PORT=$((%StartPort% + i * 2));
    TOR_CONTROL_PUBLIC_PORT=$((TOR_PUBLIC_PORT + 1));

	TOR_CONTAINER_ID="$(
		docker run \
			--detach \
	 		--name=vote-worker-tor-${i} \
	 		--net=skyvoter-worker \
			--publish=${TOR_PUBLIC_PORT}:9050 \
			--publish=${TOR_CONTROL_PUBLIC_PORT}:9051 \
			--label=voter-worker \
			docker-registry.myenv.tk/vote-worker-tor:%env.TAG%
	)"

	TOR_IP="$(
		docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' ${TOR_CONTAINER_ID}
	)"

	echo "Started \"tor\" container with ID: ${TOR_CONTAINER_ID}; IP: ${TOR_IP}; Public port: ${TOR_PUBLIC_PORT}; Public control port: ${TOR_CONTROL_PUBLIC_PORT}."

	PRIVOXY_CONTAINER_ID="$(
		docker run \
			--detach \
	 		--name=vote-worker-privoxy-${i} \
	 		--net=skyvoter-worker \
			--env TOR_IP=%env.TOR_PUBLIC_IP% \
			--env TOR_PORT=${TOR_PUBLIC_PORT} \
			--label=voter-worker \
			docker-registry.myenv.tk/vote-worker-privoxy:%env.TAG%
	)"


	PRIVOXY_IP="$(
		docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' ${PRIVOXY_CONTAINER_ID}
	)"

	echo "Started \"privoxy\" container with ID: ${PRIVOXY_CONTAINER_ID}; IP: ${PRIVOXY_IP};."

    CHROME_VNC_PORT=$((5900 + i));

    CHROME_CONTAINER_ID="$(
		docker run \
			--detach \
	 		--name=vote-worker-chrome-${i} \
	 		--net=skyvoter-worker \
			--env VNC_NO_PASSWORD=1 \
			--env http_proxy=${PRIVOXY_IP}:8118 \
			--env https_proxy=${PRIVOXY_IP}:8118 \
			--env HTTP_PROXY=${PRIVOXY_IP}:8118 \
			--env HTTPS_PROXY=${PRIVOXY_IP}:8118 \
			--publish=${CHROME_VNC_PORT}:5900 \
			--label=voter-worker \
			selenium/standalone-chrome-debug
	)"

	CHROME_IP="$(
		docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' ${CHROME_CONTAINER_ID}
	)"

	echo "Started \"chrome\" container with ID: ${CHROME_CONTAINER_ID}; IP: ${CHROME_IP}; VNC port: ${CHROME_VNC_PORT}."

	APP_CONTAINER_ID="$(
		docker run \
			--detach \
	 		--name=vote-worker-app-${i} \
	 		--net=skyvoter-worker \
			--env TOR_IP=%env.TOR_PUBLIC_IP% \
			--env TOR_PUBLIC_IP=%env.TOR_PUBLIC_IP% \
			--env TOR_PORT=${TOR_PUBLIC_PORT} \
			--env TOR_CONTROL_PORT=${TOR_CONTROL_PUBLIC_PORT} \
			--env TOR_CONTROL_PASSWORD=%env.tor_control_password% \
			--env PRIVOXY_IP=${PRIVOXY_IP} \
			--env PRIVOXY_PORT=8118 \
			--env DIRECTOR_BASE_URL=https://api.prime.mu/ \
			--env SELENIUM_BROWSER_HOST=${CHROME_IP} \
			--env SELENIUM_BROWSER_PORT=4444 \
			--env SELENIUM_BROWSER_NAME=%env.SELENIUM_BROWSER_NAME% \
			--env CAP_MONSTER_HOST=%env.CAP_MONSTER_HOST% \
			--env CAP_MONSTER_SCHEME=%env.CAP_MONSTER_SCHEME% \
			--volume /storage/docker/worker/logs:/var/www/skyvoter/var/log \
			--label=voter-worker \
			docker-registry.myenv.tk/vote-worker-app:%env.TAG%
	)"

	echo "Started \"app\" container with ID: ${APP_CONTAINER_ID};"
done